**What's using?**

#Spring 5.2.1
#Docker
#Java
#Liquibase
#Postgresql
#Hibernate

#

---

## What's about?

This all backend app in Spring is a scuffolding for Angular 8(Rest API).
WebApp consist from user site and panel admin site.
In shortcut - user is registered and he want's to order a service for his own car f.ex.repearing air conditioning or change the tire.

---

## Functionality

#register and login
#creating roles and permissions
#security JWT(permissions as claims)
#role can have more than one permissions
#secured endpoints by permissions
#adding sections, category, subcategory and service
#service provider can choose service from defined from above or he can create own service
#pagination, sort endpoints
#and more

## Docker

#data attached to Docker spec are fake(I removed for the sake of public repo)

---
