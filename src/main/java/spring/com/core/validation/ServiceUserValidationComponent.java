package spring.com.core.validation;

import spring.com.core.domain.IErrorCode;
import spring.com.core.domain.IErrorMessage;
import spring.com.core.domain.Status;
import spring.com.core.dto.Service.ServiceUserDto;
import spring.com.core.model.Service.Service;
import spring.com.core.model.Service.ServiceCategory;
import spring.com.core.model.User.UserAuth;
import spring.com.core.model.Users.UsersServices;
import spring.com.core.repository.Service.ServiceCategoryRepository;
import spring.com.core.repository.Service.ServiceRepository;
import spring.com.core.repository.User.UserRepository;
import spring.com.core.repository.Users.UsersServicesRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class ServiceUserValidationComponent {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ServiceRepository serviceRepository;

    @Autowired
    private ServiceCategoryRepository serviceCategoryRepository;

    @Autowired
    private UsersServicesRepository usersServicesRepository;

    private Status status;

    private IErrorCode code;

    private IErrorMessage message;

    public boolean isValidSection(ServiceUserDto serviceAdminDto) {

        String name = serviceAdminDto.getName();
        ServiceCategory sectionExist = serviceCategoryRepository.findByNameIgnoreCaseContaining(name);

        if (name == null) {
            status = new Status(code.EMPTY_SECTION_NAME, message.EMPTY_SECTION_NAME);
            return false;
        }
        if (sectionExist != null && sectionExist.getParent() == null) {
            status = new Status(code.EXIST_SECTION_NAME, message.EXIST_SECTION_NAME);
            return false;
        }

        return true;
    }

    public boolean isValidUpdateSection(Long id, ServiceUserDto serviceAdminDto) {

        String name = serviceAdminDto.getName();
        ServiceCategory serviceCategoryExistByName = serviceCategoryRepository.findByNameIgnoreCaseContaining(name);

        if (!serviceCategoryRepository.findById(id).isPresent()) {
            status = new Status(code.SECTION_NOT_FOUND, message.SECTION_NOT_FOUND);
            return false;
        }
        if (name == null) {
            status = new Status(code.EMPTY_SECTION_NAME, message.EMPTY_SECTION_NAME);
            return false;
        }
        if (serviceCategoryExistByName != null && serviceCategoryExistByName.getParent() == null) {
            status = new Status(code.EXIST_SECTION_NAME, message.EXIST_SECTION_NAME);
            return false;
        }
        if (serviceCategoryExistByName != null && serviceCategoryExistByName.getParent() != null) {
            status = new Status(code.EXIST_CAT_OR_SUBCAT_NAME, message.EXIST_CAT_OR_SUBCAT_NAME);
            return false;
        }

        return true;
    }

    public boolean isValidCategory(ServiceUserDto serviceAdminDto) {

        String name = serviceAdminDto.getName();
        ServiceCategory serviceCategoryExistByName = serviceCategoryRepository.findByNameIgnoreCaseContaining(name);

        if (name == null) {
            status = new Status(code.EMPTY_CAT_NAME, message.EMPTY_CAT_NAME);
            return false;
        }
        if (serviceCategoryExistByName != null && serviceCategoryExistByName.getParent() == null) {
            status = new Status(code.EXIST_SECTION_NAME, message.EXIST_SECTION_NAME);
            return false;
        }
        if (serviceCategoryExistByName != null && serviceCategoryExistByName.getParent() != null) {
            status = new Status(code.EXIST_CAT_OR_SUBCAT_NAME, message.EXIST_CAT_OR_SUBCAT_NAME);
            return false;
        }

        return true;
    }

    public boolean isValidUpdateCategory(Long id, ServiceUserDto serviceAdminDto) {

        String name = serviceAdminDto.getName();
        ServiceCategory serviceCategoryExistByName = serviceCategoryRepository.findByNameIgnoreCaseContaining(name);

        if (!serviceCategoryRepository.findById(id).isPresent()) {
            status = new Status(code.CAT_NOT_FOUND, message.CAT_NOT_FOUND);
            return false;
        }
        if (name == null) {
            status = new Status(code.EMPTY_CAT_NAME, message.EMPTY_CAT_NAME);
            return false;
        }
        if (serviceCategoryExistByName != null && serviceCategoryExistByName.getParent() == null) {
            status = new Status(code.EXIST_SECTION_NAME, message.EXIST_SECTION_NAME);
            return false;
        }
        if (serviceCategoryExistByName != null && serviceCategoryExistByName.getParent() != null) {
            status = new Status(code.EXIST_CAT_OR_SUBCAT_NAME, message.EXIST_CAT_OR_SUBCAT_NAME);
            return false;
        }

        return true;
    }

    public boolean isValidSubCategory(ServiceUserDto serviceAdminDto) {

        String name = serviceAdminDto.getName();
        ServiceCategory serviceCategoryExistByName = serviceCategoryRepository.findByNameIgnoreCaseContaining(name);

        if (name == null) {
            status = new Status(code.EMPTY_SUBCAT_NAME, message.EMPTY_SUBCAT_NAME);
            return false;
        }
        if (serviceCategoryExistByName != null && serviceCategoryExistByName.getParent() == null) {
            status = new Status(code.EXIST_SECTION_NAME, message.EXIST_SECTION_NAME);
            return false;
        }
        if (serviceCategoryExistByName != null && serviceCategoryExistByName.getParent() != null) {
            status = new Status(code.EXIST_CAT_OR_SUBCAT_NAME, message.EXIST_CAT_OR_SUBCAT_NAME);
            return false;
        }

        return true;
    }

    public boolean isValidUpdateSubCategory(Long id, ServiceUserDto serviceAdminDto) {

        String name = serviceAdminDto.getName();
        ServiceCategory serviceCategoryExistByName = serviceCategoryRepository.findByNameIgnoreCaseContaining(name);

        if (!serviceCategoryRepository.findById(id).isPresent()) {
            status = new Status(code.SUBCAT_NOT_FOUND, message.SUBCAT_NOT_FOUND);
            return false;
        }
        if (name == null) {
            status = new Status(code.EMPTY_SUBCAT_NAME, message.EMPTY_SUBCAT_NAME);
            return false;
        }
        if (serviceCategoryExistByName != null && serviceCategoryExistByName.getParent() == null) {
            status = new Status(code.EXIST_SECTION_NAME, message.EXIST_SECTION_NAME);
            return false;
        }
        if (serviceCategoryExistByName != null && serviceCategoryExistByName.getParent() != null) {
            status = new Status(code.EXIST_CAT_OR_SUBCAT_NAME, message.EXIST_CAT_OR_SUBCAT_NAME);
            return false;
        }

        return true;
    }

    public boolean isValidService(Long id, ServiceUserDto serviceAdminDto) {

        Service serviceExistByName = serviceRepository.findByNameIgnoreCaseContaining(serviceAdminDto.getName());
        Service serviceExistByDesc = serviceRepository.findByDescriptionIgnoreCaseContaining(serviceAdminDto.getDescription());

        if (!serviceCategoryRepository.findById(id).isPresent()) {
            status = new Status(code.SUBCAT_NOT_FOUND, message.SUBCAT_NOT_FOUND);
            return false;
        }
        if (serviceAdminDto.getName() == null) {
            status = new Status(code.EMPTY_SERVICE_NAME, message.EMPTY_SERVICE_NAME);
            return false;
        }
        if (serviceAdminDto.getDescription() == null) {
            status = new Status(code.EMPTY_SERVICE_DESC, message.EMPTY_SERVICE_DESC);
            return false;
        }
        if (serviceAdminDto.getPrice() == null) {
            status = new Status(code.EMPTY_SERVICE_PRICE, message.EMPTY_SERVICE_PRICE);
            return false;
        }
        if (serviceExistByName != null && serviceExistByName.getUserAuth() == null) {
            status = new Status(code.EXIST_DF_SERVICE_NAME, message.EXIST_DF_SERVICE_NAME);
            return false;
        }
        if (serviceExistByDesc != null && serviceExistByDesc.getUserAuth() == null) {
            status = new Status(code.EXIST_DF_SERVICE_DESC, message.EXIST_DF_SERVICE_DESC);
            return false;
        }

        return true;
    }

    public boolean isValidUpdateService(Long id, ServiceUserDto serviceAdminDto) {

        Service serviceExistByName = serviceRepository.findByNameIgnoreCaseContaining(serviceAdminDto.getName());
        Service serviceExistByDesc = serviceRepository.findByDescriptionIgnoreCaseContaining(serviceAdminDto.getDescription());

        if (!serviceRepository.findById(id).isPresent()) {
            status = new Status(code.SERVICE_NOT_FOUND, message.SERVICE_NOT_FOUND);
            return false;
        }
        if (serviceAdminDto.getName() == null) {
            status = new Status(code.EMPTY_SERVICE_NAME, message.EMPTY_SERVICE_NAME);
            return false;
        }
        if (serviceAdminDto.getDescription() == null) {
            status = new Status(code.EMPTY_SERVICE_DESC, message.EMPTY_SERVICE_DESC);
            return false;
        }
        if (serviceAdminDto.getPrice() == null) {
            status = new Status(code.EMPTY_SERVICE_PRICE, message.EMPTY_SERVICE_PRICE);
            return false;
        }
        if (serviceExistByName != null && serviceExistByName.getUserAuth() == null) {
            status = new Status(code.EXIST_DF_SERVICE_NAME, message.EXIST_DF_SERVICE_NAME);
            return false;
        }
        if (serviceExistByDesc != null && serviceExistByDesc.getUserAuth() == null) {
            status = new Status(code.EXIST_DF_SERVICE_DESC, message.EXIST_DF_SERVICE_DESC);
            return false;
        }

        return true;
    }

    public boolean isValidServiceForUser(Long userId, Long serviceId) {

        UserAuth userAuthExist = userRepository.findById(userId).get();
        Service serviceExist = serviceRepository.findById(serviceId).get();

        UsersServices usersServices = usersServicesRepository.findByUserAuthAndService(userAuthExist, serviceExist);

        if (!userRepository.findById(userId).isPresent()) {
            status = new Status(code.USER_NOT_FOUND, message.USER_NOT_FOUND);
            return false;
        }
        if (!serviceRepository.findById(serviceId).isPresent()) {
            status = new Status(code.SERVICE_NOT_FOUND, message.SERVICE_NOT_FOUND);
            return false;
        }
        if (usersServices != null) {
            status = new Status(code.EXIST_SERVICE_USER_ACCOUNT, message.EXIST_SERVICE_USER_ACCOUNT);
            return false;
        }

        return true;
    }

    public boolean isValidUpdateServiceForUser(Long userId, Long serviceId) {

        if (!userRepository.findById(userId).isPresent()) {
            status = new Status(code.USER_NOT_FOUND, message.USER_NOT_FOUND);
            return false;
        }
        if (!serviceRepository.findById(serviceId).isPresent()) {
            status = new Status(code.SERVICE_NOT_FOUND, message.SERVICE_NOT_FOUND);
            return false;
        }
        return true;
    }

    public boolean isValidForeignService(Long userId, Long subcatId, ServiceUserDto serviceAdminDto) {

        Service existingServiceName = serviceRepository.findByNameIgnoreCaseContaining(serviceAdminDto.getName());
        Service existingServiceDesc = serviceRepository.findByDescriptionIgnoreCaseContaining(serviceAdminDto.getDescription());
        UserAuth userAuth = userRepository.findById(userId).get();

        if (existingServiceName != null && existingServiceName.getUserAuth() == null) {
            status = new Status(code.EXIST_DF_SERVICE_NAME, message.EXIST_DF_SERVICE_NAME);
            return false;
        }
        if (existingServiceName != null && existingServiceName.getUserAuth() != null && existingServiceName.getUserAuth() == userAuth) {
            status = new Status(code.EXIST_SERVICE_NAME_USER_ACCOUNT, message.EXIST_SERVICE_NAME_USER_ACCOUNT);
            return false;
        }
        if (existingServiceDesc != null && existingServiceDesc.getUserAuth() == null) {
            status = new Status(code.EXIST_DF_SERVICE_DESC, message.EXIST_DF_SERVICE_DESC);
            return false;
        }
        if (!userRepository.findById(userId).isPresent()) {
            status = new Status(code.USER_NOT_FOUND, message.USER_NOT_FOUND);
            return false;
        }
        if (!serviceCategoryRepository.findById(subcatId).isPresent()) {
            status = new Status(code.SUBCAT_NOT_FOUND, message.SUBCAT_NOT_FOUND);
            return false;
        }
        if (serviceAdminDto.getName() == null) {
            status = new Status(code.EMPTY_SERVICE_NAME, message.EMPTY_SERVICE_NAME);
            return false;
        }
        if (serviceAdminDto.getDescription() == null) {
            status = new Status(code.EMPTY_SERVICE_DESC, message.EMPTY_SERVICE_DESC);
            return false;
        }
        if (serviceAdminDto.getPrice() == null) {
            status = new Status(code.EMPTY_SERVICE_PRICE, message.EMPTY_SERVICE_PRICE);
            return false;
        }

        return true;
    }

    public boolean isValidUpdateForeignService(Long userId, Long serviceId, ServiceUserDto serviceAdminDto) {

        Service existingServiceName = serviceRepository.findByNameIgnoreCaseContaining(serviceAdminDto.getName());
        Service existingServiceDesc = serviceRepository.findByDescriptionIgnoreCaseContaining(serviceAdminDto.getDescription());

        if (!userRepository.findById(userId).isPresent()) {
            status = new Status(code.USER_NOT_FOUND, message.USER_NOT_FOUND);
            return false;
        }
        if (!serviceRepository.findById(serviceId).isPresent()) {
            status = new Status(code.SERVICE_NOT_FOUND, message.SERVICE_NOT_FOUND);
            return false;
        }
        if (serviceAdminDto.getName() == null) {
            status = new Status(code.EMPTY_SERVICE_NAME, message.EMPTY_SERVICE_NAME);
            return false;
        }
        if (serviceAdminDto.getDescription() == null) {
            status = new Status(code.EMPTY_SERVICE_DESC, message.EMPTY_SERVICE_DESC);
            return false;
        }
        if (serviceAdminDto.getPrice() == null) {
            status = new Status(code.EMPTY_SERVICE_PRICE, message.EMPTY_SERVICE_PRICE);
            return false;
        }
        if (existingServiceName != null && existingServiceName.getUserAuth() == null) {
            status = new Status(code.EXIST_DF_SERVICE_NAME, message.EXIST_DF_SERVICE_NAME);
            return false;
        }
        if (existingServiceDesc != null && existingServiceDesc.getUserAuth() == null) {
            status = new Status(code.EXIST_DF_SERVICE_DESC, message.EXIST_DF_SERVICE_DESC);
            return false;
        }

        return true;
    }


    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
