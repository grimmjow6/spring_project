package spring.com.core.validation;

import spring.com.core.domain.IErrorCode;
import spring.com.core.domain.IErrorMessage;
import spring.com.core.domain.Status;
import spring.com.core.dto.PasswordResetDto;
import spring.com.core.dto.User.*;
import spring.com.core.model.Access.Role;
import spring.com.core.model.User.UserAuth;
import spring.com.core.repository.Access.RoleRepository;
import spring.com.core.repository.User.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class UserValidationComponent {

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    private Status status;

    private IErrorCode code;

    private IErrorMessage message;

    private boolean ifUserExist(Long id) {
        return userRepository.findById(id).isPresent();
    }

    public boolean isValidUpdateUser(Long id, UserDto userDto) {

        if (!ifUserExist(id)) {
            status = new Status(code.USER_NOT_FOUND, message.USER_NOT_FOUND);
            return false;
        }
        if (userDto.getName() == null) {
            status = new Status(code.EMPTY_NAME, message.EMPTY_NAME);
            return false;
        }
        if (userDto.getSurname() == null) {
            status = new Status(code.EMPTY_SURNAME, message.EMPTY_SURNAME);
            return false;
        }
        if (userDto.getPhone() == null) {
            status = new Status(code.EMPTY_PHONE, message.EMPTY_PHONE);
            return false;
        }

        return true;
    }

    public boolean isValidUpdateCompanyDetails(Long id, UserCompanyDetailsDto userCompanyDetailsDto) {

        if (!ifUserExist(id)) {
            status = new Status(code.USER_NOT_FOUND, message.USER_NOT_FOUND);
            return false;
        }
        if (userCompanyDetailsDto.getCompanyName() == null) {
            status = new Status(code.EMPTY_COMPANY_NAME, message.EMPTY_COMPANY_NAME);
            return false;
        }
        if (userCompanyDetailsDto.getNip() == null) {
            status = new Status(code.EMPTY_NIP, message.EMPTY_NIP);
            return false;
        }

        return true;
    }

    public boolean isValidUpdateAddress(Long id, UserAddressDto userAddressDto) {

        if (!ifUserExist(id)) {
            status = new Status(code.USER_NOT_FOUND, message.USER_NOT_FOUND);
            return false;
        }
        if (userAddressDto.getStreet() == null) {
            status = new Status(code.EMPTY_STREET, message.EMPTY_STREET);
            return false;
        }
        if (userAddressDto.getCountry() == null) {
            status = new Status(code.EMPTY_COUNTRY, message.EMPTY_COUNTRY);
            return false;
        }
        if (userAddressDto.getPostcode() == null) {
            status = new Status(code.EMPTY_POSTCODE, message.EMPTY_POSTCODE);
            return false;
        }
        if (userAddressDto.getCity() == null) {
            status = new Status(code.EMPTY_CITY, message.EMPTY_CITY);
            return false;
        }
        return true;
    }

    public boolean isValidUpdateDetails(Long id, UserDetailsDto userDetailsDto) {

        if (!ifUserExist(id)) {
            status = new Status(code.USER_NOT_FOUND, message.USER_NOT_FOUND);
            return false;
        }
        if (userDetailsDto.getName() == null) {
            status = new Status(code.EMPTY_NAME, message.EMPTY_NAME);
            return false;
        }
        if (userDetailsDto.getSurname() == null) {
            status = new Status(code.EMPTY_SURNAME, message.EMPTY_SURNAME);
            return false;
        }

        return true;
    }

    public boolean isValidUpdatePassword(Long id, PasswordResetDto passwordResetDto) {

        UserAuth userAuth = userRepository.findById(id).get();

        if (!ifUserExist(id)) {
            status = new Status(code.USER_NOT_FOUND, message.USER_NOT_FOUND);
            return false;
        }
        if (!passwordEncoder.matches(passwordResetDto.getOldPassword(), userAuth.getPassword())) {
            status = new Status(code.PASSWORD_CURRENT_INCORRECT, message.PASSWORD_CURRENT_INCORRECT);
            return false;
        }
        if (!passwordResetDto.getNewPassword().equals(passwordResetDto.getConfirmPassword())) {
            status = new Status(code.PASSWORDS_NOT_FIT, message.PASSWORDS_NOT_FIT);
            return false;
        }
        if (passwordResetDto.getOldPassword() == null) {
            status = new Status(code.PASSWORD_CURRENT_EMPTY, message.PASSWORD_CURRENT_EMPTY);
            return false;
        }
        if (passwordResetDto.getNewPassword() == null) {
            status = new Status(code.PASSWORD_NEW_EMPTY, message.PASSWORD_NEW_EMPTY);
            return false;
        }
        if (passwordResetDto.getConfirmPassword() == null) {
            status = new Status(code.PASSWORD_REPEAT_EMPTY, message.PASSWORD_REPEAT_EMPTY);
            return false;
        }

        return true;
    }

    public boolean isValidDeleteUser(Long id) {

        if (!ifUserExist(id)) {
            status = new Status(code.USER_NOT_FOUND, message.USER_NOT_FOUND);
            return false;
        }

        return true;
    }

    public boolean isValidLogin(UserLoginDto userLoginDto) {

        UserAuth existing = userRepository.findByEmail(userLoginDto.getEmail());

        if (userLoginDto.getEmail() == null) {
            status = new Status(code.EMAIL_EMPTY, message.EMAIL_EMPTY);
            return false;
        }

        if (existing == null) {
            status = new Status(code.EMAIL_NOT_FOUND, message.EMAIL_NOT_FOUND);
            return false;
        }

        if (userLoginDto.getPassword() == null) {
            status = new Status(code.PASSWORD_EMPTY, message.PASSWORD_EMPTY);
            return false;
        }

        if (!passwordEncoder.matches(userLoginDto.getPassword(), existing.getPassword())) {
            status = new Status(code.CREDENTIAL_INCORRECT, message.CREDENTIAL_INCORRECT);
            return false;
        }

        return true;
    }

    public boolean isValidRegistration(UserRegistrationDto userRegistrationDto) {
        UserAuth existing = userRepository.findByEmail(userRegistrationDto.getEmail());
        Long userRoleId = userRegistrationDto.getRoleId();

        if (existing != null) {
            status = new Status(code.EXIST_EMAIL, message.EXIST_EMAIL);
            return false;
        }
        if (userRegistrationDto.getEmail() == null) {
            status = new Status(code.EMAIL_EMPTY, message.EMAIL_EMPTY);
            return false;
        }
        if (userRegistrationDto.getPassword() == null) {
            status = new Status(code.PASSWORD_EMPTY, message.PASSWORD_EMPTY);
            return false;
        }

        if (userRoleId != null) {
            Role role = roleRepository.findById(userRoleId).get();

            if (role.getName().equals("ROLE_SERVICE_PROVIDER") && userRegistrationDto.getCompanyName() == null) {
                status = new Status(code.EMPTY_COMPANY_NAME, message.EMPTY_COMPANY_NAME);
                return false;
            }
            if (role.getName().equals("ROLE_SERVICE_PROVIDER") && userRegistrationDto.getCompanyCity() == null) {
                status = new Status(code.EMPTY_CITY, message.EMPTY_CITY);
                return false;
            }
            if (role.getName().equals("ROLE_SERVICE_PROVIDER") && userRegistrationDto.getPhone() == null) {
                status = new Status(code.EMPTY_PHONE, message.EMPTY_PHONE);
                return false;
            }
            if (role.getName().equals("ROLE_SERVICE_PROVIDER") && userRegistrationDto.getNip() == null) {
                status = new Status(code.EMPTY_NIP, message.EMPTY_NIP);
                return false;
            }
        } else {

            if (userRegistrationDto.getType().equals("serviceProvider") && userRegistrationDto.getCompanyName() == null) {
                status = new Status(code.EMPTY_COMPANY_NAME, message.EMPTY_COMPANY_NAME);
                return false;
            }
            if (userRegistrationDto.getType().equals("serviceProvider") && userRegistrationDto.getCompanyCity() == null) {
                status = new Status(code.EMPTY_CITY, message.EMPTY_CITY);
                return false;
            }
            if (userRegistrationDto.getType().equals("serviceProvider") && userRegistrationDto.getPhone() == null) {
                status = new Status(code.EMPTY_PHONE, message.EMPTY_PHONE);
                return false;
            }
            if (userRegistrationDto.getType().equals("ROLE_SERVICE_PROVIDER") && userRegistrationDto.getNip() == null) {
                status = new Status(code.EMPTY_NIP, message.EMPTY_NIP);
                return false;
            }
        }
        return true;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
