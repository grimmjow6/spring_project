package spring.com.core.validation;

import spring.com.core.domain.IErrorCode;
import spring.com.core.domain.IErrorMessage;
import spring.com.core.domain.Status;
import spring.com.core.dto.Access.AccessDto;
import spring.com.core.model.Access.Permission;
import spring.com.core.model.Access.Role;
import spring.com.core.repository.Access.PermissionRepository;
import spring.com.core.repository.Access.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class AccessValidationComponent {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PermissionRepository permissionRepository;

    private Status status;

    private IErrorCode code;

    private IErrorMessage message;

    public boolean isValidRole(AccessDto accessDto) {

        String name = accessDto.getName();
        Role roleExist = roleRepository.findByNameIgnoreCaseContaining(name);

        if (name == null) {
            status = new Status(code.EMPTY_ROLE_NAME, message.EMPTY_ROLE_NAME);
            return false;
        }
        if (roleExist != null) {
            status = new Status(code.EXIST_ROLE, message.EXIST_ROLE);
            return false;
        }

        return true;
    }

    public boolean isValidUpdateRole(Long id, AccessDto accessDto) {

        String name = accessDto.getName();
        Role roleExist = roleRepository.findByNameIgnoreCaseContaining(name);

        if (!roleRepository.findById(id).isPresent()) {
            status = new Status(code.ROLE_NOT_FOUND, message.ROLE_NOT_FOUND);
            return false;
        }
        if (name == null) {
            status = new Status(code.EMPTY_ROLE_NAME, message.EMPTY_ROLE_NAME);
            return false;
        }
        if (roleExist != null && !id.equals(roleExist.getId())) {
            status = new Status(code.EXIST_ROLE, message.EXIST_ROLE);
            return false;
        }

        return true;
    }

    public boolean isValidPermission(AccessDto accessDto) {

        String name = accessDto.getName();
        Permission permissionExist = permissionRepository.findByNameIgnoreCaseContaining(name);

        if (name == null) {
            status = new Status(code.EMPTY_PERMISSION_NAME, message.EMPTY_PERMISSION_NAME);
            return false;
        }
        if (permissionExist != null) {
            status = new Status(code.PERSMISSION_EXIST, message.PERSMISSION_EXIST);
            return false;
        }

        return true;
    }

    public boolean isValidUpdatePermission(Long id, AccessDto accessDto) {

        String name = accessDto.getName();
        Permission permissionExist = permissionRepository.findByNameIgnoreCaseContaining(name);

        if (!permissionRepository.findById(id).isPresent()) {
            status = new Status(code.PERMISSION_NOT_FOUND, message.PERMISSION_NOT_FOUND);
            return false;
        }
        if (name == null) {
            status = new Status(code.EMPTY_PERMISSION_NAME, message.EMPTY_PERMISSION_NAME);
            return false;
        }
        if (permissionExist != null && !id.equals(permissionExist.getId())) {
            status = new Status(code.PERSMISSION_EXIST, message.PERSMISSION_EXIST);
            return false;
        }

        return true;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
