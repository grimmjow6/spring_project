package spring.com.core.controller.Access;

import spring.com.core.domain.IErrorCode;
import spring.com.core.domain.IErrorMessage;
import spring.com.core.domain.Status;
import spring.com.core.dto.Access.AccessDto;
import spring.com.core.model.Access.DeviceData;
import spring.com.core.model.Access.Permission;
import spring.com.core.model.Access.Role;
import spring.com.core.repository.Access.DeviceDataRepository;
import spring.com.core.repository.Access.PermissionRepository;
import spring.com.core.repository.Access.RoleRepository;
import spring.com.core.service.Access.IAccessService;
import spring.com.core.validation.AccessValidationComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/access")
public class AccessController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private DeviceDataRepository deviceDataRepository;

    @Autowired
    private IAccessService IAccessService;

    @Autowired
    private AccessValidationComponent accessValidationComponent;

    private Status status;

    private IErrorCode code;

    private IErrorMessage message;

    @GetMapping("/device-data")
    public Page<DeviceData> retrieveAllDeviceData(@PageableDefault(page = 0, size = 15) Pageable pageable) {
        Page<DeviceData> page = deviceDataRepository.findAll(pageable);
        return page;
    }

    @PreAuthorize("hasAuthority('GET_ROLE_PRIVILEGE')")
    @GetMapping("/roles")
    public Page<Role> retrieveAllRoles(@PageableDefault(page = 0, size = 15) Pageable pageable) {
        Page<Role> page = roleRepository.findAll(pageable);
        return page;
    }

    @PreAuthorize("hasAuthority('ADD_ROLE_PRIVILEGE')")
    @PostMapping("/roles")
    public Status saveRole(@RequestBody AccessDto accessDto) {

        if (!accessValidationComponent.isValidRole(accessDto)) {
            return accessValidationComponent.getStatus();
        }

        try {
            IAccessService.saveRole(accessDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }

    @PreAuthorize("hasAuthority('GET_ROLE_PRIVILEGE')")
    @GetMapping("/roles/{id}")
    public Role retrieveRole(@PathVariable Long id) {
        Role role = roleRepository.findById(id).get();
        return role;
    }

    @PreAuthorize("hasAuthority('EDIT_ROLE_PRIVILEGE')")
    @PutMapping("/roles/{id}")
    public Status updateRole(@PathVariable Long id, @RequestBody AccessDto accessDto) {

        if (!accessValidationComponent.isValidUpdateRole(id, accessDto)) {
            return accessValidationComponent.getStatus();
        }

        try {
            IAccessService.updateRole(id, accessDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }


    @PreAuthorize("hasAuthority('GET_PERMISSION_PRIVILEGE')")
    @GetMapping("/permissions")
    public Page<Permission> retrieveAllPermissions(@PageableDefault(page = 0, size = 15) Pageable pageable) {
        Page<Permission> page = permissionRepository.findAll(pageable);
        return page;
    }

    @PreAuthorize("hasAuthority('ADD_PERMISSION_PRIVILEGE')")
    @PostMapping("/permissions")
    public Status savePermission(@RequestBody AccessDto accessDto) {

        if (!accessValidationComponent.isValidPermission(accessDto)) {
            return accessValidationComponent.getStatus();
        }

        try {
            IAccessService.savePermission(accessDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }

    @PreAuthorize("hasAuthority('GET_PERMISSION_PRIVILEGE')")
    @GetMapping("/permissions/{id}")
    public Permission retrievePermission(@PathVariable Long id) {
        Permission permission = permissionRepository.findById(id).get();
        return permission;
    }

    @PreAuthorize("hasAuthority('EDIT_PERMISSION_PRIVILEGE')")
    @PutMapping("/permissions/{id}")
    public Status updatePermission(@PathVariable Long id, @RequestBody AccessDto accessDto) {

        if (!accessValidationComponent.isValidUpdatePermission(id, accessDto)) {
            return accessValidationComponent.getStatus();
        }

        try {
            IAccessService.updatePermission(id, accessDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }

}
