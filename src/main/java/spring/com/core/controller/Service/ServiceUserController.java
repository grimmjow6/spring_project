package spring.com.core.controller.Service;

import spring.com.core.domain.IErrorCode;
import spring.com.core.domain.IErrorMessage;
import spring.com.core.domain.Status;
import spring.com.core.dto.Service.ServiceUserDto;
import spring.com.core.model.Service.ServiceCategory;
import spring.com.core.model.Service.Service;
import spring.com.core.model.User.UserAuth;
import spring.com.core.model.Users.UsersServices;
import spring.com.core.repository.Service.ServiceCategoryRepository;
import spring.com.core.repository.Service.ServiceRepository;
import spring.com.core.repository.User.UserRepository;
import spring.com.core.repository.Users.UsersServicesRepository;
import spring.com.core.service.Service.IServiceUser;
import spring.com.core.validation.ServiceUserValidationComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@SuppressWarnings("Duplicates")
@RestController
@RequestMapping("/v1")
public class ServiceUserController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ServiceRepository serviceRepository;

    @Autowired
    private ServiceCategoryRepository serviceCategoryRepository;

    @Autowired
    private UsersServicesRepository usersServicesRepository;

    @Autowired
    private ServiceUserValidationComponent serviceUserValidationComponent;

    @Autowired
    private IServiceUser service;

    private Status status;

    private IErrorCode code;

    private IErrorMessage message;

    //    @PreAuthorize("hasAuthority('ADD_USER_SERVICE_PRIVILEGE')")
    @GetMapping("/users/{id}/services")
    public Page<UsersServices> retrieveSpecificServicesForUser(
            @PageableDefault(page = 0, size = 15) Pageable pageable,
            @PathVariable("id") long id,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam(value = "type", required = false) String type) {

        Page<UsersServices> page = null;
        String active = "active";
        String non_active = "non-active";
        String defined = "defined";
        String foreign = "foreign";

        if (status.isEmpty() && type.isEmpty()) {
            page = usersServicesRepository.findByUserAuthId(id, pageable);
        } else if (status.equalsIgnoreCase(active) && type.isEmpty()) {
            page = usersServicesRepository.findByUserAuthIdAndActiveTrue(id, pageable);
        } else if (status.equalsIgnoreCase(non_active) && type.isEmpty()) {
            page = usersServicesRepository.findByUserAuthIdAndActiveFalse(id, pageable);
        } else if (type.equalsIgnoreCase(defined) && status.isEmpty()) {
            page = usersServicesRepository.findByUserAuthIdAndService_UserAuthIsNull(id, pageable);
        } else if (type.equalsIgnoreCase(foreign) && status.isEmpty()) {
            page = usersServicesRepository.findByUserAuthIdAndService_UserAuthIsNotNull(id, pageable);
        } else if (status.equalsIgnoreCase(active) && type.equalsIgnoreCase(defined)) {
            page = usersServicesRepository.findByUserAuthIdAndActiveTrueAndService_UserAuthIsNull(id, pageable);
        } else if (status.equalsIgnoreCase(active) && type.equalsIgnoreCase(foreign)) {
            page = usersServicesRepository.findByUserAuthIdAndActiveTrueAndService_UserAuthIsNotNull(id, pageable);
        } else if (status.equalsIgnoreCase(non_active) && type.equalsIgnoreCase(defined)) {
            page = usersServicesRepository.findByUserAuthIdAndActiveFalseAndService_UserAuthIsNull(id, pageable);
        } else if (status.equalsIgnoreCase(non_active) && type.equalsIgnoreCase(foreign)) {
            page = usersServicesRepository.findByUserAuthIdAndActiveFalseAndService_UserAuthIsNotNull(id, pageable);
        }

        return page;
    }

    @GetMapping("/subcategories/{id}/services")
    public Page<Service> retrieveSpecificServicesForSubcategory(@PageableDefault(page = 0, size = 15) Pageable pageable,
                                                                @PathVariable("id") long id,
                                                                @RequestParam(value = "type", required = false) String type) {

        Page<Service> page = null;
        String defined = "defined";
        String foreign = "foreign";

        if (type.isEmpty()) {
            page = serviceRepository.findAllByServiceCategory_Id(id, pageable);
        } else if (type.equalsIgnoreCase(defined)) {
            page = serviceRepository.findByUserAuthIsNullAndServiceCategory_Id(id, pageable);
        } else if (type.equalsIgnoreCase(foreign)) {
            page = serviceRepository.findByUserAuthIsNotNullAndServiceCategory_Id(id, pageable);
        }
        return page;
    }

    @GetMapping("/sections/{id}/categories")
    public List<ServiceCategory> retrieveAllCategoryForSection(@PathVariable long id) {
        return serviceCategoryRepository.findAllByParent_Id(id);
    }

    @GetMapping("/categories/{id}/subcategories")
    public List<ServiceCategory> retrieveAllSubcategoryForCategory(@PathVariable Long id) {
        return serviceCategoryRepository.findAllByParent_Id(id);
    }

    @GetMapping("/services")
    public Page<Service> retrieveSpecificServices(@PageableDefault(page = 0, size = 15) Pageable pageable,
                                                  @RequestParam(value = "type", required = false) String type) {

        Page<Service> page = null;
        String defined = "defined";
        String foreign = "foreign";

        if (type.isEmpty()) {
            page = serviceRepository.findAll(pageable);
        } else if (type.equalsIgnoreCase(defined)) {
            page = serviceRepository.findByUserAuthIsNull(pageable);
        } else if (type.equalsIgnoreCase(foreign)) {
            page = serviceRepository.findByUserAuthIsNotNull(pageable);
        }
        return page;
    }

    @GetMapping("/services/{id}")
    public Service findByIdSpecificService(@PathVariable Long id,
                                           @RequestParam(value = "type", required = false) String type) {
        Service service = null;
        String defined = "defined";
        String foreign = "foreign";

        if (type.isEmpty()) {
            service = serviceRepository.findById(id).get();
        } else if (type.equalsIgnoreCase(defined)) {
            service = serviceRepository.findByIdAndUserAuthIsNull(id);
        } else if (type.equalsIgnoreCase(foreign)) {
            service = serviceRepository.findByIdAndUserAuthIsNotNull(id);
        }
        return service;
    }

    @GetMapping("/users/{userId}/subcategories/{subcatId}/services")
    public Page<UsersServices> retrieveSpecificServicesForUserAndSubcat(
            @PageableDefault(page = 0, size = 15) Pageable pageable,
            @PathVariable("userId") long userId,
            @PathVariable("subcatId") long subcatId,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam(value = "type", required = false) String type) {

        Page<UsersServices> page = null;
        String active = "active";
        String non_active = "non-active";
        String defined = "defined";
        String foreign = "foreign";

        if (status.isEmpty() && type.isEmpty()) {
            page = usersServicesRepository.findByUserAuthIdAndService_ServiceCategoryId(userId, subcatId, pageable);
        } else if (status.equalsIgnoreCase(active) && type.isEmpty()) {
            page = usersServicesRepository.findByUserAuthIdAndActiveTrueAndService_ServiceCategoryId(userId, subcatId, pageable);
        } else if (status.equalsIgnoreCase(non_active) && type.isEmpty()) {
            page = usersServicesRepository.findByUserAuthIdAndActiveFalseAndService_ServiceCategoryId(userId, subcatId, pageable);
        } else if (type.equalsIgnoreCase(defined) && status.isEmpty()) {
            page = usersServicesRepository.findByUserAuthIdAndService_ServiceCategoryIdAndService_UserAuthIsNull(userId, subcatId, pageable);
        } else if (type.equalsIgnoreCase(foreign) && status.isEmpty()) {
            page = usersServicesRepository.findByUserAuthIdAndService_ServiceCategoryIdAndService_UserAuthIsNotNull(userId, subcatId, pageable);
        } else if (status.equalsIgnoreCase(active) && type.equalsIgnoreCase(defined)) {
            page = usersServicesRepository.findByUserAuthIdAndActiveTrueAndService_ServiceCategoryIdAndService_UserAuthIsNull(userId, subcatId, pageable);
        } else if (status.equalsIgnoreCase(active) && type.equalsIgnoreCase(foreign)) {
            page = usersServicesRepository.findByUserAuthIdAndActiveTrueAndService_ServiceCategoryIdAndService_UserAuthIsNotNull(userId, subcatId, pageable);
        } else if (status.equalsIgnoreCase(non_active) && type.equalsIgnoreCase(defined)) {
            page = usersServicesRepository.findByUserAuthIdAndActiveFalseAndService_ServiceCategoryIdAndService_UserAuthIsNull(userId, subcatId, pageable);
        } else if (status.equalsIgnoreCase(non_active) && type.equalsIgnoreCase(foreign)) {
            page = usersServicesRepository.findByUserAuthIdAndActiveFalseAndService_ServiceCategoryIdAndService_UserAuthIsNotNull(userId, subcatId, pageable);
        }

        return page;
    }

    @GetMapping("/sections")
    public List<ServiceCategory> retrieveAllSection() {
        return serviceCategoryRepository.findByParentIsNull();
    }

    @PreAuthorize("hasAuthority('ADD_SECTION_PRIVILEGE')")
    @PostMapping("/sections")
    public Status saveSection(@RequestBody ServiceUserDto serviceAdminDto) {

        if (!serviceUserValidationComponent.isValidSection(serviceAdminDto)) {
            return serviceUserValidationComponent.getStatus();
        }

        try {
            service.saveSection(serviceAdminDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }

    @GetMapping("/sections/{id}")
    public ServiceCategory findByIdSection(@PathVariable Long id) {
        ServiceCategory serviceCategory = serviceCategoryRepository.findById(id).get();
        return serviceCategory;
    }

    @PreAuthorize("hasAuthority('EDIT_SECTION_PRIVILEGE')")
    @PutMapping("/sections/{id}")
    public Status updateSection(@PathVariable Long id, @RequestBody ServiceUserDto serviceAdminDto) {

        if (!serviceUserValidationComponent.isValidUpdateSection(id, serviceAdminDto)) {
            return serviceUserValidationComponent.getStatus();
        }

        try {
            service.updateSection(id, serviceAdminDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }

    @GetMapping("/categories")
    public List<ServiceCategory> retrieveAllCategory() {
        return serviceCategoryRepository.findByParentIsNotNullAndParent_Parent_IdIsNull();
    }

    @PreAuthorize("hasAuthority('ADD_CAT_PRIVILEGE')")
    @PostMapping("/sections/{id}/categories")
    public Status saveCategory(@PathVariable Long id, @RequestBody ServiceUserDto serviceAdminDto) {

        if (!serviceUserValidationComponent.isValidCategory(serviceAdminDto)) {
            return serviceUserValidationComponent.getStatus();
        }

        try {
            service.saveCategory(id, serviceAdminDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }

    @GetMapping("/categories/{id}")
    public ServiceCategory findByIdCategory(@PathVariable Long id) {
        ServiceCategory serviceCategory = serviceCategoryRepository.findById(id).get();
        return serviceCategory;
    }

    @PreAuthorize("hasAuthority('EDIT_CAT_PRIVILEGE')")
    @PutMapping("/categories/{id}")
    public Status updateCategory(@PathVariable Long id, @RequestBody ServiceUserDto serviceAdminDto) {

        if (!serviceUserValidationComponent.isValidUpdateCategory(id, serviceAdminDto)) {
            return serviceUserValidationComponent.getStatus();
        }

        try {
            service.updateCategory(id, serviceAdminDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }

    @GetMapping("/subcategories")
    public List<ServiceCategory> retrieveAllSubcategory() {
        return serviceCategoryRepository.findByParentIsNotNull();
    }

    @PreAuthorize("hasAuthority('ADD_SUBCAT_PRIVILEGE')")
    @PostMapping("/categories/{id}/subcategories")
    public Status saveSubcategory(@PathVariable Long id, @RequestBody ServiceUserDto serviceAdminDto) {

        if (!serviceUserValidationComponent.isValidSubCategory(serviceAdminDto)) {
            return serviceUserValidationComponent.getStatus();
        }

        try {
            service.saveSubcategory(id, serviceAdminDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }

    @GetMapping("/subcategories/{id}")
    public ServiceCategory findByIdSubcategory(@PathVariable Long id) {
        ServiceCategory serviceCategory = serviceCategoryRepository.findById(id).get();
        return serviceCategory;
    }

    @PreAuthorize("hasAuthority('EDIT_SUBCAT_PRIVILEGE')")
    @PutMapping("/subcategories/{id}")
    public Status updateSubcategory(@PathVariable Long id, @RequestBody ServiceUserDto serviceAdminDto) {

        if (!serviceUserValidationComponent.isValidUpdateSubCategory(id, serviceAdminDto)) {
            return serviceUserValidationComponent.getStatus();
        }

        try {
            service.updateSubcategory(id, serviceAdminDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }


    @PreAuthorize("hasAuthority('ADD_SERVICE_PRIVILEGE')")
    @PostMapping("/subcategories/{id}/services")
    public Status saveService(@PathVariable Long id, @RequestBody ServiceUserDto serviceAdminDto) {

        if (!serviceUserValidationComponent.isValidService(id, serviceAdminDto)) {
            return serviceUserValidationComponent.getStatus();
        }

        try {
            service.saveService(id, serviceAdminDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }


    @PreAuthorize("hasAuthority('EDIT_SERVICE_PRIVILEGE')")
    @PutMapping("/services/{id}")
    public Status updateService(@PathVariable Long id, @RequestBody ServiceUserDto serviceAdminDto) {

        if (!serviceUserValidationComponent.isValidUpdateService(id, serviceAdminDto)) {
            return serviceUserValidationComponent.getStatus();
        }

        try {
            service.updateService(id, serviceAdminDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }

    @PreAuthorize("hasAuthority('ADD_USER_SERVICE_PRIVILEGE')")
    @PostMapping("/services/{serviceId}/users/{userId}")
    public Status saveDefinedServiceForUser(@PathVariable("userId") Long userId, @PathVariable("serviceId") Long serviceId) {

        if (!serviceUserValidationComponent.isValidServiceForUser(userId, serviceId)) {
            return serviceUserValidationComponent.getStatus();
        }

        try {
            service.saveDefinedServiceForUser(userId, serviceId);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }

    @PreAuthorize("hasAuthority('EDIT_USER_SERVICE_PRIVILEGE')")
    @GetMapping("/services/{serviceId}/users/{userId}")
    public UsersServices findByIdDefinedServiceForUser(@PathVariable("userId") Long userId, @PathVariable("serviceId") Long serviceId) {
        UserAuth userAuthExist = userRepository.findById(userId).get();
        Service serviceExist = serviceRepository.findById(serviceId).get();

        UsersServices displayUsersServices = usersServicesRepository.findByUserAuthAndService(userAuthExist, serviceExist);

        return displayUsersServices;
    }

    @PreAuthorize("hasAuthority('EDIT_USER_SERVICE_PRIVILEGE')")
    @PutMapping("/services/{serviceId}/users/{userId}")
    public Status updateDefinedServiceForUser(@PathVariable("userId") Long userId, @PathVariable("serviceId") Long serviceId, @RequestBody ServiceUserDto serviceDto) {


        if (!serviceUserValidationComponent.isValidUpdateServiceForUser(userId, serviceId)) {
            return serviceUserValidationComponent.getStatus();
        }

        try {
            service.updateDefinedServicePrice(userId, serviceId, serviceDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }

    @PreAuthorize("hasAuthority('ADD_SERVICE_PRIVILEGE')")
    @PostMapping("/subcategories/{subcatId}/users/{userId}/services")
    public Status saveForeignService(@PathVariable("userId") Long userId, @PathVariable("subcatId") Long subcatId, @RequestBody ServiceUserDto serviceDto) {

        if (!serviceUserValidationComponent.isValidForeignService(userId, subcatId, serviceDto)) {
            return serviceUserValidationComponent.getStatus();
        }

        try {
            service.saveForeignService(userId, subcatId, serviceDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }

    @PreAuthorize("hasAuthority('EDIT_USER_SERVICE_PRIVILEGE') or hasAuthority('READ_ONLY_PRIVILEGE')")
    @GetMapping("/services-foreign/{serviceId}/users/{userId}")
    public UsersServices findByIdForeignServiceForUser(@PathVariable("userId") Long userId, @PathVariable("serviceId") Long serviceId) {
        UserAuth userAuthExist = userRepository.findById(userId).get();
        Service serviceExist = serviceRepository.findByIdAndUserAuthId(serviceId, userId);

        UsersServices displayUsersServices = usersServicesRepository.findByUserAuthAndService(userAuthExist, serviceExist);

        return displayUsersServices;
    }

    @PreAuthorize("hasAuthority('EDIT_USER_SERVICE_PRIVILEGE')")
    @PutMapping("/services-foreign/{serviceId}/users/{userId}")
    public Status updateForeignService(@PathVariable("userId") Long userId, @PathVariable("serviceId") Long serviceId, @RequestBody ServiceUserDto serviceDto) {

        if (!serviceUserValidationComponent.isValidUpdateForeignService(userId, serviceId, serviceDto)) {
            return serviceUserValidationComponent.getStatus();
        }

        try {
            service.updateForeignService(userId, serviceId, serviceDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }
}