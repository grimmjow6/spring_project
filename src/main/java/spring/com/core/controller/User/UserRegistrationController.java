package spring.com.core.controller.User;

import spring.com.core.domain.IErrorCode;
import spring.com.core.domain.IErrorMessage;
import spring.com.core.service.User.AuthService;
import spring.com.core.domain.Status;
import spring.com.core.dto.User.UserLoginDto;
import spring.com.core.dto.User.UserRegistrationDto;
import spring.com.core.model.User.UserAuth;
import spring.com.core.security.JwtTokenUtil;
import spring.com.core.service.Access.DeviceDataService;
import spring.com.core.service.User.IUserService;
import spring.com.core.validation.UserValidationComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/v1/api")
public class UserRegistrationController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private IUserService IUserService;

    @Autowired
    private DeviceDataService deviceDataService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private AuthService authService;

    private Status status;

    private IErrorCode code;

    private IErrorMessage message;

    @Autowired
    private UserValidationComponent userValidationComponent;

    private void loginNotification(Authentication authentication, HttpServletRequest request) {
        try {
            if (authentication.getPrincipal() != null) {
                deviceDataService.verifyDevice(authentication, request);
            }
        } catch (Exception e) {
            logger.error("An error occurred while verifying device or location: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @PostMapping("/login")
    public Status login(@RequestBody UserLoginDto userDto, HttpServletRequest request) {

        UserAuth existing = IUserService.findByEmail(userDto.getEmail());

        if (!userValidationComponent.isValidLogin(userDto)) {
            return userValidationComponent.getStatus();
        }

        String username = userDto.getEmail();
        String password = userDto.getPassword();

        try {
            final Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            username,
                            password
                    )
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);
            loginNotification(authentication, request);

            final String token = jwtTokenUtil.generateToken(authentication);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE, existing.getId(), token);

            return this.authService.addRolesToStatus(existing, status);

        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }

    @PostMapping("/users")
    public Status registerUserAccount(@RequestBody UserRegistrationDto userDto) {

        if (!userValidationComponent.isValidRegistration(userDto)) {
            return userValidationComponent.getStatus();
        }

        try {
            IUserService.save(userDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }

    }

}
