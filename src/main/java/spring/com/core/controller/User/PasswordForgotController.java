package spring.com.core.controller.User;

import spring.com.core.domain.IErrorCode;
import spring.com.core.domain.IErrorMessage;
import spring.com.core.domain.Status;
import spring.com.core.dto.PasswordForgotDto;
import spring.com.core.model.Mail;
import spring.com.core.model.User.UserAuth;
import spring.com.core.service.User.EmailService;
import spring.com.core.service.User.IUserService;
import spring.com.core.utils.PasswordGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/v1/api/remind-password")
public class PasswordForgotController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private IUserService IUserService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private PasswordGenerator passwordGenerator;

    private Status status;

    private IErrorCode code;

    private IErrorMessage message;

    @ModelAttribute("forgotPasswordForm")
    public PasswordForgotDto forgotPasswordDto() {
        return new PasswordForgotDto();
    }

    @PostMapping
    @ResponseBody
    public Status processForgotPasswordForm(@RequestBody PasswordForgotDto form,
                                            BindingResult result,
                                            HttpServletRequest request) {

        UserAuth user = IUserService.findByEmail(form.getEmail());

        if (user == null) {
            status = new Status(code.EMAIL_NOT_FOUND, message.EMAIL_NOT_FOUND);
            return status;
        }

        String pass = passwordGenerator.generatePassayPassword();

        if (pass == null) {
            status = new Status(code.PASSWORD_NOT_GENERATED, message.PASSWORD_NOT_GENERATED);
            return status;
        }
        IUserService.changeUserPassword(user, passwordEncoder.encode(pass));

        try {
            Mail mail = new Mail();
            mail.setFrom("no-reply@memorynotfound.com");
            mail.setTo(user.getEmail());
            mail.setSubject("Prośba o zmianę hasła do systemu Autonova");

            Map<String, Object> model = new HashMap<>();
            model.put("user", user);
            model.put("signature", "https://autonova.com");
            model.put("newPassword", pass);
            String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
            model.put("resetUrl", url + "/api/login");
            mail.setModel(model);
            emailService.sendEmail(mail);
            status = new Status(1, "Nowe hasło zostało wygenerowane i email z przypomnieniem został wysłany");

        } catch (Exception ex) {
            status = new Status(2, "Nowe hasło zostało wygenerowane ale email nie został wysłany", ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }

        if (result.hasErrors()) {
            status.status = 0;
        }

        return status;

    }

}