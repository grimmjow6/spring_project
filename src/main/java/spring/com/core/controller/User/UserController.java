package spring.com.core.controller.User;

import spring.com.core.domain.IErrorCode;
import spring.com.core.domain.IErrorMessage;
import spring.com.core.domain.Status;
import spring.com.core.dto.PasswordResetDto;
import spring.com.core.dto.User.UserAddressDto;
import spring.com.core.dto.User.UserCompanyDetailsDto;
import spring.com.core.dto.User.UserDetailsDto;
import spring.com.core.dto.User.UserDto;
import spring.com.core.model.Access.Role;
import autonova.com.core.model.User.*;
import spring.com.core.model.User.UserAddress;
import spring.com.core.model.User.UserAuth;
import spring.com.core.model.User.UserCompanyDetails;
import spring.com.core.model.User.UserDetails;
import spring.com.core.repository.Access.RoleRepository;
import autonova.com.core.repository.User.*;
import spring.com.core.validation.UserValidationComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import spring.com.core.repository.User.AddressRepository;
import spring.com.core.repository.User.CompanyDetailsRepository;
import spring.com.core.repository.User.DetailsRepository;
import spring.com.core.repository.User.UserRepository;

@RestController
@RequestMapping("/v1/users")
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private spring.com.core.service.User.IUserService IUserService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private CompanyDetailsRepository companyDetailsRepository;

    @Autowired
    private DetailsRepository detailsRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private UserValidationComponent userValidationComponent;

    private Status status;

    private IErrorMessage message;

    private IErrorCode code;

    @PreAuthorize("hasAuthority('GET_USER_PRIVILEGE')")
    @GetMapping("/{id}")
    public UserAuth findByIdUser(@PathVariable long id) {
        UserAuth user = userRepository.findById(id).get();
        return user;
    }

    @PreAuthorize("hasAuthority('EDIT_USER_PRIVILEGE')")
    @PutMapping("/{id}")
    public Status update(@PathVariable Long id, @RequestBody UserDto userDto) {

        if (!userValidationComponent.isValidUpdateUser(id, userDto)) {
            return userValidationComponent.getStatus();
        }

        try {
            IUserService.update(id, userDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }

    @PreAuthorize("hasAuthority('GET_USER_PRIVILEGE')")
    @GetMapping("/{id}/company-details")
    public UserCompanyDetails findByIdUserCompanyDetails(@PathVariable long id) {
        UserAuth user = userRepository.findById(id).get();
        return user.getCompanyDetails();
    }

    @PreAuthorize("hasAuthority('EDIT_USER_PRIVILEGE')")
    @PutMapping("/{id}/company-details")
    public Status updateCompanyDetails(@PathVariable Long id, @RequestBody UserCompanyDetailsDto userCompanyDetailsDto) {

        if (!userValidationComponent.isValidUpdateCompanyDetails(id, userCompanyDetailsDto)) {
            return userValidationComponent.getStatus();
        }

        try {
            IUserService.updateCompanyDetails(id, userCompanyDetailsDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }

    @PreAuthorize("hasAuthority('GET_USER_PRIVILEGE')")
    @GetMapping("/{id}/addresses")
    public UserAddress findByIdUserAddress(@PathVariable long id) {
        UserAuth user = userRepository.findById(id).get();
        return user.getAddress();
    }

    @PreAuthorize("hasAuthority('EDIT_USER_PRIVILEGE')")
    @PutMapping("/{id}/addresses")
    public Status updateAddress(@PathVariable Long id, @RequestBody UserAddressDto userAddressDto) {

        if (!userValidationComponent.isValidUpdateAddress(id, userAddressDto)) {
            return userValidationComponent.getStatus();
        }

        try {
            IUserService.updateAddress(id, userAddressDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }

    @PreAuthorize("hasAuthority('GET_USER_PRIVILEGE')")
    @GetMapping("/{id}/details")
    public UserDetails findByIdUserDetails(@PathVariable long id) {
        UserAuth user = userRepository.findById(id).get();
        return user.getDetails();
    }

    @PreAuthorize("hasAuthority('EDIT_USER_PRIVILEGE')")
    @PutMapping("/{id}/details")
    public Status updateDetails(@PathVariable Long id, @RequestBody UserDetailsDto userDetailsDto) {

        if (!userValidationComponent.isValidUpdateDetails(id, userDetailsDto)) {
            return userValidationComponent.getStatus();
        }

        try {
            IUserService.updateDetails(id, userDetailsDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }

    @PreAuthorize("hasAuthority('EDIT_USER_PRIVILEGE')")
    @PutMapping("/{id}/reset-password")
    public Status updatePassword(@PathVariable Long id, @RequestBody PasswordResetDto passwordResetDto) {

        if (!userValidationComponent.isValidUpdatePassword(id, passwordResetDto)) {
            return userValidationComponent.getStatus();
        }

        try {
            IUserService.updatePassword(id, passwordResetDto);
            status = new Status(code.SUCCESS_TRUE, message.SUCCESS_TRUE);
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }

    @PreAuthorize("hasAuthority('GET_ALL_USER_PRIVILEGE')")
    @GetMapping
    public Page<UserAuth> retrieveAllUsers(
            @PageableDefault(page = 0, size = 15) Pageable pageable,
            @RequestParam(value = "clientNumber", required = false) String clientNumber,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "phone", required = false) String phone,
            @RequestParam(value = "roles", required = false) String roles) {
        return IUserService.retrieveAllUsers(pageable, clientNumber, email, phone, roles);
    }

    @PreAuthorize("hasAuthority('GET_ALL_USER_PRIVILEGE')")
    @GetMapping("/addresses")
    public Page<UserAddress> retrieveAllAddress(@PageableDefault(page = 0, size = 15) Pageable pageable) {
        Page<UserAddress> page = addressRepository.findAll(pageable);
        return page;
    }

    @PreAuthorize("hasAuthority('GET_ALL_USER_PRIVILEGE')")
    @GetMapping("/company-details")
    public Page<UserCompanyDetails> retrieveAllCompanyDetails(@PageableDefault(page = 0, size = 15) Pageable pageable) {
        Page<UserCompanyDetails> page = companyDetailsRepository.findAll(pageable);
        return page;
    }

    @PreAuthorize("hasAuthority('GET_ALL_USER_PRIVILEGE')")
    @GetMapping("/details")
    public Page<UserDetails> retrieveAllDetails(@PageableDefault(page = 0, size = 15) Pageable pageable) {
        Page<UserDetails> page = detailsRepository.findAll(pageable);
        return page;
    }

    @PreAuthorize("hasAuthority('GET_ALL_USER_PRIVILEGE')")
    @GetMapping("/participants")
    public Page<UserAuth> retrieveAllParticipants(@PageableDefault(page = 0, size = 15) Pageable pageable) {
        Role role = roleRepository.findByName("ROLE_PARTICIPANT");
        Page<UserAuth> page = userRepository.findByRoles(role, pageable);
        return page;
    }

    @PreAuthorize("hasAuthority('GET_ALL_USER_PRIVILEGE')")
    @GetMapping("/providers")
    public Page<UserAuth> retrieveAllServiceProviders(@PageableDefault(page = 0, size = 15) Pageable pageable) {
        Role role = roleRepository.findByName("ROLE_SERVICE_PROVIDER");
        Page<UserAuth> page = userRepository.findByRoles(role, pageable);
        return page;
    }

    @PreAuthorize("hasAuthority('DELETE_USER_PRIVILEGE')")
    @GetMapping("/{id}/delete")
    public UserAuth findByIdUserDelete(@PathVariable long id) {
        UserAuth user = userRepository.findById(id).get();
        return user;
    }

    @PreAuthorize("hasAuthority('DELETE_USER_PRIVILEGE')")
    @PutMapping("/{id}/delete")
    public Status deleteUser(@PathVariable Long id) {

        if (!userValidationComponent.isValidDeleteUser(id)) {
            return userValidationComponent.getStatus();
        }

        try {
            IUserService.deleteUser(id);
            status = new Status(1, "Użytkownik został usunięty");
            return status;
        } catch (Exception ex) {
            status = new Status(code.DATABASE_ERROR, message.DATABASE_ERROR, ex.getMessage());
            logger.error(getClass().getSimpleName(), ex);
            return status;
        }
    }
}
