package spring.com.core.security;

public class Constants {
    public static final long ACCESS_TOKEN_VALIDITY_MILLISECONDS = 300000;
    public static final String SIGNING_KEY = "/B?E(H+MbQeThWmZq4t7w!z%C&F)J@NcRfUjXn2r5u8x/A?D(G-KaPdSgVkYp3s6";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String AUTHORITIES_KEY = "scopes";
}
