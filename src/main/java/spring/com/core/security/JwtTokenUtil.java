package spring.com.core.security;

import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.KeyUse;
import io.jsonwebtoken.*;
import org.mitre.jose.jwk.ECKeyMaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.stream.Collectors;

import static spring.com.core.security.Constants.*;
import static com.nimbusds.jose.jwk.ECKey.Curve.P_256;

@Component
public class JwtTokenUtil implements Serializable {
    @Autowired
    private Environment env;

    @Autowired
    private static final Logger logger = LoggerFactory.getLogger(JwtTokenUtil.class);

    private JWK jwk = ECKeyMaker.make(P_256, KeyUse.SIGNATURE, JWSAlgorithm.HS512, null);

    public String getUserIdFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(SIGNING_KEY)
                .parseClaimsJws(token)
                .getBody();

        return claims.getSubject();
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(SIGNING_KEY).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            logger.error("Invalid JWT signature: " + ex.getMessage());
        } catch (MalformedJwtException ex) {
            logger.error("Invalid JWT token: " + ex.getMessage());
        } catch (ExpiredJwtException ex) {
            logger.error("Expired JWT token: " + ex.getMessage());
        } catch (UnsupportedJwtException ex) {
            logger.error("Unsupported JWT token: " + ex.getMessage());
        } catch (IllegalArgumentException ex) {
            logger.error("JWT claims string is empty: " + ex.getMessage());
        }
        return false;
    }

    public String generateToken(Authentication authentication) {

        final String authorities = authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","));
        return Jwts.builder()
                .setSubject(authentication.getName())
                .claim(AUTHORITIES_KEY, authorities)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + (new Long(env.getProperty("jwt_session_time")) * 1000 * 60)  ))
                .signWith(SignatureAlgorithm.HS512, SIGNING_KEY)
                .compact();
    }
}
