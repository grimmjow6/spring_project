package spring.com.core.model.Car;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "car_fuel")
public class CarFuel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    private String name;

    @ManyToOne
    @JoinColumn(name = "car_serie_id", nullable = false)
    private CarSerie carSerie;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CarSerie getCarSerie() {
        return carSerie;
    }

    public void setCarSerie(CarSerie carSerie) {
        this.carSerie = carSerie;
    }
}
