package spring.com.core.model.User;

import spring.com.core.model.Access.Role;
import spring.com.core.model.Users.UsersServices;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;

@Entity
@Data
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "email"), schema = "public")
public class UserAuth {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String email;

    @JsonIgnore
    private String password;

    private String clientNumber;
    private String phone;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm")
//    @JsonIgnore
    private LocalDateTime createdAt;

    //    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @JsonIgnore
    private LocalDateTime updatedAt;

    @JsonIgnore
    private LocalDateTime deletedAt;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_details_id", referencedColumnName = "id")
    private UserDetails details;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_address_id", referencedColumnName = "id")
    private UserAddress address;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_company_details_id", referencedColumnName = "id")
    private UserCompanyDetails companyDetails;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(
                    name = "user_auth_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"))
    private Set<Role> roles = new HashSet<Role>();

    @OneToMany(
            mappedBy = "userAuth",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<UsersServices> usersServices = new ArrayList<>();

    public UserAuth() {
    }

    public UserAuth(String email, String password, String clientNumber, LocalDateTime createdAt, Set<Role> roles) {
        this.email = email;
        this.password = password;
        this.clientNumber = clientNumber;
        this.createdAt = createdAt;
        this.roles = roles;
    }

    public UserAuth(String email, String password, String clientNumber, LocalDateTime createdAt, LocalDateTime updatedAt, UserAddress address, UserDetails details, UserCompanyDetails companyDetails, Set<Role> roles) {
        this.email = email;
        this.password = password;
        this.clientNumber = clientNumber;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.address = address;
        this.details = details;
        this.companyDetails = companyDetails;
        this.roles = roles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LocalDateTime getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(LocalDateTime deletedAt) {
        this.deletedAt = deletedAt;
    }

    public UserDetails getDetails() {
        return details;
    }

    public void setDetails(UserDetails details) {
        this.details = details;
    }

    public UserAddress getAddress() {
        return address;
    }

    public void setAddress(UserAddress address) {
        this.address = address;
    }

    public UserCompanyDetails getCompanyDetails() {
        return companyDetails;
    }

    public void setCompanyDetails(UserCompanyDetails companyDetails) {
        this.companyDetails = companyDetails;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public List<UsersServices> getUsersServices() {
        return usersServices;
    }

    public void setUsersServices(List<UsersServices> usersServices) {
        this.usersServices = usersServices;
    }

}
