package spring.com.core.model.Users;

import spring.com.core.model.Car.CarEngine;
import spring.com.core.model.User.UserAuth;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "users_cars")
public class UsersCars {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("user_auth_id")
//    @JoinColumn(name = "user_auth_id")
    private UserAuth userAuth;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("car_engine_id")
//    @JoinColumn(name = "car_id")
    private CarEngine car;

    @JsonIgnore
    private LocalDateTime createdAt;

    @JsonIgnore
    private LocalDateTime deletedAt;

    private UsersCars() {}

    public UsersCars(UserAuth userAuth, CarEngine car, LocalDateTime createdAt) {
        this.userAuth = userAuth;
        this.car = car;
        this.createdAt = createdAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserAuth getUserAuth() {
        return userAuth;
    }

    public void setUserAuth(UserAuth userAuth) {
        this.userAuth = userAuth;
    }

    public CarEngine getCar() {
        return car;
    }

    public void setCar(CarEngine car) {
        this.car = car;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(LocalDateTime deletedAt) {
        this.deletedAt = deletedAt;
    }
}
