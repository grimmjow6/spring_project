package spring.com.core.service.Access;

import spring.com.core.dto.Access.AccessDto;
import spring.com.core.model.Access.Permission;
import spring.com.core.model.Access.Role;
import spring.com.core.repository.Access.PermissionRepository;
import spring.com.core.repository.Access.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class AccessServiceImpl implements IAccessService {

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private RoleRepository roleRepository;

    private LocalDateTime date = LocalDateTime.now();

    @Override
    public Permission savePermission(AccessDto accessDto) {
        String name = accessDto.getName();

        Permission permission = new Permission(name, date);
        return permissionRepository.save(permission);
    }

    @Override
    public Permission updatePermission(Long id, AccessDto accessDto) {
        Permission permission = permissionRepository.findById(id).get();
        String name = accessDto.getName();
        permission.setName(name);
        permission.setUpdatedAt(date);

        return permissionRepository.save(permission);
    }

    @Override
    public Role saveRole(AccessDto accessDto) {
        String name = accessDto.getName();
        List<Permission> list = new ArrayList<>();
        for (Long permission : accessDto.getPermissions()
        ) {
            Permission permission1 = permissionRepository.findById(permission).get();
            list.add(permission1);
        }
        Role role = new Role(name, list, date);
        return roleRepository.save(role);
    }

    @Override
    public Role updateRole(Long id, AccessDto accessDto) {
        Role role = roleRepository.findById(id).get();
        List<Permission> list = new ArrayList<>();
        String name = accessDto.getName();

        role.setName(name);

        for (Long permission : accessDto.getPermissions()
        ) {
            Permission permission1 = permissionRepository.findById(permission).get();
            list.add(permission1);
        }

        if (list != role.getPermissions())
            role.setPermissions(list);

        role.setUpdatedAt(date);
        return roleRepository.save(role);
    }

}
