package spring.com.core.service.Access;

import spring.com.core.model.Access.DeviceData;
import spring.com.core.repository.Access.DeviceDataRepository;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import ua_parser.Client;
import ua_parser.Parser;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.LocalDateTime;

import static java.util.Objects.nonNull;

@Component
public class DeviceDataService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static final String UNKNOWN = "UNKNOWN";

    private LocalDateTime date = LocalDateTime.now();

    @Autowired
    private DeviceDataRepository deviceMetadataRepository;

    @Autowired
    private DeviceDataService deviceDataService;

    @Autowired
    private DatabaseReader databaseReader;

    @Autowired
    private Parser parser;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private MessageSource messages;

    public DeviceDataService(DeviceDataRepository deviceMetadataRepository,
                             DatabaseReader databaseReader,
                             Parser parser,
                             JavaMailSender mailSender,
                             MessageSource messages) {
        this.deviceMetadataRepository = deviceMetadataRepository;
        this.databaseReader = databaseReader;
        this.parser = parser;
        this.mailSender = mailSender;
        this.messages = messages;
    }

    public void verifyDevice(Authentication user, HttpServletRequest request) throws IOException, GeoIp2Exception {

        String ip = extractIp(request);
//        String location = getIpLocation(ip);

        String deviceDetails = getDeviceDetails(request.getHeader("user-agent"));
        logger.info("szczegóły: " + deviceDetails);

        DeviceData existingDevice = deviceMetadataRepository.findByUsernameAndIp(user.getName(), ip);

        if (existingDevice == null) {
//            unknownDeviceNotification(deviceDetails, location, ip, user.getEmail(), request.getLocale());

            DeviceData deviceMetadata = new DeviceData();
            deviceMetadata.setUsername(user.getName());
            deviceMetadata.setIp(ip);
            deviceMetadata.setDeviceDetails(deviceDetails);
            deviceMetadata.setLastLoggedIn(date);
            deviceMetadataRepository.save(deviceMetadata);
        } else {
            existingDevice.setLastLoggedIn(date);
            deviceMetadataRepository.save(existingDevice);
        }
    }

    private String extractIp(HttpServletRequest request) {
        String clientIp;
        String clientXForwardedForIp = request.getHeader("x-forwarded-for");
        if (nonNull(clientXForwardedForIp)) {
            clientIp = parseXForwardedHeader(clientXForwardedForIp);
        } else {
            clientIp = request.getRemoteAddr();
        }

        return clientIp;
    }

    private String parseXForwardedHeader(String header) {
        return header.split(" *, *")[0];
    }

    private String getDeviceDetails(String userAgent) {
        String deviceDetails = UNKNOWN;

        Client client = parser.parse(userAgent);
        if (nonNull(client)) {
            deviceDetails = client.userAgent.family + " " + client.userAgent.major + "." + client.userAgent.minor +
                    " - " + client.os.family + " " + client.os.major + "." + client.os.minor;
        }
        logger.info("szczegóły w środku: " + deviceDetails);

        return deviceDetails;
    }

//    private String getIpLocation(String ip) throws IOException, GeoIp2Exception {
//
//        String location = UNKNOWN;
//
//        InetAddress ipAddress = InetAddress.getByName(ip);
//
//        CityResponse cityResponse = databaseReader.city(ipAddress);
//        if (nonNull(cityResponse) &&
//                nonNull(cityResponse.getCity()) &&
//                !Strings.isNullOrEmpty(cityResponse.getCity().getName())) {
//
//            location = cityResponse.getCity().getName();
//        } else {
//            location = "undefined";
//        }
//
//        return location;
//    }

//    private DeviceData findExistingDevice(String username, String deviceDetails) {
//
//        List<DeviceData> knownDevices = deviceMetadataRepository.findByUsername(username);
//
//        for (DeviceData existingDevice : knownDevices) {
//            if (existingDevice.getDeviceDetails().equals(deviceDetails)) {
//                return existingDevice;
//            }
//        }
//
//        return null;
//    }
}
