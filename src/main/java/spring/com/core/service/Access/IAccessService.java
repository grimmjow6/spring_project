package spring.com.core.service.Access;

import spring.com.core.dto.Access.AccessDto;
import spring.com.core.model.Access.Permission;
import spring.com.core.model.Access.Role;

public interface IAccessService {

    Permission savePermission(AccessDto accessDto);

    Permission updatePermission(Long id, AccessDto accessDto);

    Role saveRole(AccessDto accessDto);

    Role updateRole(Long id, AccessDto accessDto);

}
