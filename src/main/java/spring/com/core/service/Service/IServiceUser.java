package spring.com.core.service.Service;

import spring.com.core.dto.Service.ServiceUserDto;
import spring.com.core.model.Service.ServiceCategory;
import spring.com.core.model.Service.Service;
import spring.com.core.model.Users.UsersServices;

public interface IServiceUser {

    Service updateForeignService(Long userId, Long serviceId, ServiceUserDto serviceDto);

    Service saveService(Long id, ServiceUserDto serviceAdminDto);

    Service updateService(Long id, ServiceUserDto serviceAdminDto);

    ServiceCategory saveSection(ServiceUserDto serviceAdminDto);

    ServiceCategory saveCategory(Long id, ServiceUserDto serviceAdminDto);

    ServiceCategory saveSubcategory(Long id, ServiceUserDto serviceAdminDto);

    ServiceCategory updateSection(Long id, ServiceUserDto serviceAdminDto);

    ServiceCategory updateCategory(Long id, ServiceUserDto serviceAdminDto);

    ServiceCategory updateSubcategory(Long id, ServiceUserDto serviceAdminDto);

    UsersServices saveDefinedServiceForUser(Long userId, Long serviceId);

    UsersServices saveForeignService(Long userId, Long categoryId, ServiceUserDto serviceDto);

    UsersServices updateDefinedServicePrice(Long userId, Long serviceId, ServiceUserDto serviceDto);
}
