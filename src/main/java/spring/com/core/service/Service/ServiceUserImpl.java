package spring.com.core.service.Service;

import spring.com.core.dto.Service.ServiceUserDto;
import spring.com.core.model.Service.ServiceCategory;
import spring.com.core.model.Service.Service;
import spring.com.core.model.User.UserAuth;
import spring.com.core.model.Users.UsersServices;
import spring.com.core.repository.Service.ServiceCategoryRepository;
import spring.com.core.repository.Service.ServiceRepository;
import spring.com.core.repository.User.UserRepository;
import spring.com.core.repository.Users.UsersServicesRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
public class ServiceUserImpl implements IServiceUser {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ServiceRepository serviceRepository;

    @Autowired
    private ServiceCategoryRepository serviceCategoryRepository;

    @Autowired
    private UsersServicesRepository usersServicesRepository;

    private LocalDateTime date = LocalDateTime.now();

    public UsersServices saveDefinedServiceForUser(Long userId, Long serviceId) {
        UserAuth user = userRepository.findById(userId).get();
        Service service = serviceRepository.findById(serviceId).get();

        UsersServices usersServices = new UsersServices(user, service, date);
        usersServices.setServiceUserPrice(0.00);
        usersServices.setActive(true);

        return usersServicesRepository.save(usersServices);
    }

    public UsersServices saveForeignService(Long userId, Long categoryId, ServiceUserDto serviceDto) {
        UserAuth user = userRepository.findById(userId).get();
        ServiceCategory serviceCategory = serviceCategoryRepository.findById(categoryId).get();
        Service service = new Service();

        service.setName(serviceDto.getName());
        service.setDescription(serviceDto.getDescription());
        service.setPrice(null);
        service.setActive(false);
        service.setCreatedAt(date);
        service.setServiceCategory(serviceCategory);
        service.setUserAuth(user);
        serviceRepository.save(service);

        UsersServices usersServices = new UsersServices(user, service, date);
        usersServices.setActive(false);
        usersServices.setServiceUserPrice(serviceDto.getPrice());

        return usersServicesRepository.save(usersServices);
    }

    public Service updateForeignService(Long userId, Long serviceId, ServiceUserDto serviceDto) {
        Service service = serviceRepository.findById(serviceId).get();

        service.setName(serviceDto.getName());
        service.setDescription(serviceDto.getDescription());
        service.setPrice(serviceDto.getPrice());
        service.setUpdatedAt(date);

        return serviceRepository.save(service);
    }

    public UsersServices updateActiveService(Long userId, Long serviceId, ServiceUserDto serviceDto) {
        Service service = serviceRepository.findById(serviceId).get();
        UserAuth userAuth = userRepository.findById(userId).get();
        UsersServices usersServices = usersServicesRepository.findByUserAuthAndService(userAuth, service);

        usersServices.setActive(serviceDto.isActive());

        if (!serviceDto.isActive()) {
            usersServices.setDeletedAt(date);
        } else {
            usersServices.setUpdatedAt(date);
            usersServices.setDeletedAt(null);
        }

        return usersServicesRepository.save(usersServices);
    }

    public UsersServices updateDefinedServicePrice(Long userId, Long serviceId, ServiceUserDto serviceDto) {
        Service service = serviceRepository.findById(serviceId).get();
        UserAuth userAuth = userRepository.findById(userId).get();
        UsersServices usersServices = usersServicesRepository.findByUserAuthAndService(userAuth, service);

        usersServices.setServiceUserPrice(serviceDto.getPrice());
        usersServices.setActive(serviceDto.isActive());
        usersServices.setUpdatedAt(date);

        return usersServicesRepository.save(usersServices);
    }

    @Override
    public ServiceCategory saveSection(ServiceUserDto serviceAdminDto) {
        String name = serviceAdminDto.getName();
        boolean active = serviceAdminDto.isActive();

        ServiceCategory serviceCategory = new ServiceCategory(name, null, active, date);

        return serviceCategoryRepository.save(serviceCategory);
    }

    @Override
    public ServiceCategory saveCategory(Long id, ServiceUserDto serviceAdminDto) {
        String name = serviceAdminDto.getName();
        boolean active = serviceAdminDto.isActive();

        ServiceCategory sectionExist = serviceCategoryRepository.findById(id).get();
        ServiceCategory serviceCategory = new ServiceCategory(name, sectionExist, active, date);

        return serviceCategoryRepository.save(serviceCategory);
    }

    @Override
    public ServiceCategory saveSubcategory(Long id, ServiceUserDto serviceAdminDto) {
        String name = serviceAdminDto.getName();
        boolean active = serviceAdminDto.isActive();

        ServiceCategory category = serviceCategoryRepository.findById(id).get();
        ServiceCategory serviceCategory = new ServiceCategory(name, category, active, date);

        return serviceCategoryRepository.save(serviceCategory);
    }

    @Override
    public Service saveService(Long id, ServiceUserDto serviceAdminDto) {
        Service service = new Service();
        ServiceCategory serviceCategory = serviceCategoryRepository.findById(id).get();

        service.setName(serviceAdminDto.getName());
        service.setDescription(serviceAdminDto.getDescription());
        service.setPrice(serviceAdminDto.getPrice());
        service.setActive(serviceAdminDto.isActive());
        service.setServiceCategory(serviceCategory);
        service.setUserAuth(null);
        service.setCreatedAt(date);

        return serviceRepository.save(service);
    }

    @Override
    public ServiceCategory updateSection(Long id, ServiceUserDto serviceAdminDto) {
        ServiceCategory section = serviceCategoryRepository.findById(id).get();
        boolean active = serviceAdminDto.isActive();
        List<ServiceCategory> categories = serviceCategoryRepository.findAllByParent_Id(id);
        List<Long> ids = categories.stream().map(ServiceCategory::getId).collect(Collectors.toList());

        section.setName(serviceAdminDto.getName());
        section.setActive(active);

        if (!active) {
            section.setDeletedAt(date);

            for (ServiceCategory category : categories) {
                category.setActive(false);
                category.setDeletedAt(date);
                serviceCategoryRepository.save(category);
            }

            for (Long subcategory : ids
            ) {
                List<ServiceCategory> subcats = serviceCategoryRepository.findAllByParent_Id(subcategory);
                for (ServiceCategory subcat : subcats
                ) {
                    subcat.setActive(false);
                    subcat.setDeletedAt(date);
                    serviceCategoryRepository.save(subcat);
                }
                List<Long> idsSubcats = subcats.stream().map(ServiceCategory::getId).collect(Collectors.toList());

                for (Long dfService : idsSubcats
                ) {
                    List<Service> services = serviceRepository.findAllByServiceCategory_IdAndUserAuthIsNull(dfService);
                    for (Service service : services
                    ) {
                        service.setActive(false);
                        service.setDeletedAt(date);
                        serviceRepository.save(service);
                    }
                }
            }
        } else {
            section.setUpdatedAt(date);
            section.setDeletedAt(null);

            for (ServiceCategory category : categories) {
                category.setActive(true);
                category.setDeletedAt(null);
                category.setUpdatedAt(date);
                serviceCategoryRepository.save(category);
            }

            for (Long subcategory : ids
            ) {
                List<ServiceCategory> subcats = serviceCategoryRepository.findAllByParent_Id(subcategory);

                for (ServiceCategory subcat : subcats
                ) {
                    subcat.setActive(true);
                    subcat.setDeletedAt(null);
                    subcat.setUpdatedAt(date);
                    serviceCategoryRepository.save(subcat);
                }

                List<Long> idsSubcats = subcats.stream().map(ServiceCategory::getId).collect(Collectors.toList());

                for (Long dfService : idsSubcats
                ) {
                    List<Service> services = serviceRepository.findAllByServiceCategory_IdAndUserAuthIsNull(dfService);
                    for (Service service : services
                    ) {
                        service.setActive(true);
                        service.setDeletedAt(null);
                        service.setUpdatedAt(date);
                        serviceRepository.save(service);
                    }
                }
            }
        }
        return serviceCategoryRepository.save(section);
    }

    @Override
    public ServiceCategory updateCategory(Long id, ServiceUserDto serviceAdminDto) {
        ServiceCategory category = serviceCategoryRepository.findById(id).get();
        List<ServiceCategory> subcats = serviceCategoryRepository.findAllByParent_Id(id);
        List<Long> ids = subcats.stream().map(ServiceCategory::getId).collect(Collectors.toList());

        boolean active = serviceAdminDto.isActive();

        category.setName(serviceAdminDto.getName());
        category.setActive(active);

        if (!active) {
            category.setDeletedAt(date);

            for (ServiceCategory subcat : subcats) {
                subcat.setActive(false);
                subcat.setDeletedAt(date);
                serviceCategoryRepository.save(subcat);
            }

            for (Long dfService : ids
            ) {
                List<Service> services = serviceRepository.findAllByServiceCategory_IdAndUserAuthIsNull(dfService);

                for (Service service : services
                ) {
                    service.setActive(false);
                    service.setDeletedAt(date);
                    serviceRepository.save(service);
                }

            }
        } else {
            category.setUpdatedAt(date);
            category.setDeletedAt(null);

            for (ServiceCategory subcat : subcats) {
                subcat.setActive(true);
                subcat.setDeletedAt(null);
                subcat.setUpdatedAt(date);
                serviceCategoryRepository.save(subcat);
            }

            for (Long dfService : ids
            ) {
                List<Service> services = serviceRepository.findAllByServiceCategory_IdAndUserAuthIsNull(dfService);
                for (Service service : services
                ) {
                    service.setActive(true);
                    service.setDeletedAt(null);
                    service.setUpdatedAt(date);
                    serviceRepository.save(service);
                }
            }
        }
        return serviceCategoryRepository.save(category);
    }

    @Override
    public ServiceCategory updateSubcategory(Long id, ServiceUserDto serviceAdminDto) {
        ServiceCategory subcategory = serviceCategoryRepository.findById(id).get();
        List<Service> services = serviceRepository.findAllByServiceCategory_Id(id);
        boolean active = serviceAdminDto.isActive();
        subcategory.setActive(active);

        if (!active) {
            subcategory.setDeletedAt(date);

            for (Service service : services) {
                service.setActive(false);
                service.setDeletedAt(date);
                serviceRepository.save(service);
            }
        } else {
            subcategory.setUpdatedAt(date);
            subcategory.setDeletedAt(null);

            for (Service service : services) {
                service.setActive(true);
                service.setDeletedAt(null);
                service.setUpdatedAt(date);
                serviceRepository.save(service);
            }
        }
        return serviceCategoryRepository.save(subcategory);
    }

    @Override
    public Service updateService(Long id, ServiceUserDto serviceAdminDto) {
        Service service = serviceRepository.findById(id).get();
        boolean active = serviceAdminDto.isActive();

        service.setName(serviceAdminDto.getName());
        service.setDescription(serviceAdminDto.getDescription());
        service.setPrice(serviceAdminDto.getPrice());
        service.setActive(active);

        if (!active) {
            service.setDeletedAt(date);
        } else {
            service.setUpdatedAt(date);
            service.setDeletedAt(null);
        }

        return serviceRepository.save(service);
    }
}
