package spring.com.core.service.User;

import spring.com.core.domain.Status;
import spring.com.core.model.User.UserAuth;
import spring.com.core.model.Access.Role;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class AuthService {
    public Status addRolesToStatus(UserAuth userAuth, Status status) {
        if (userAuth != null && status != null) {
            if (status.roles == null) {
                ArrayList<String> roles = new ArrayList<String>();
                for (Role userRole: userAuth.getRoles()) {
                    roles.add(userRole.getName());
                }
                status.roles = new String[roles.size()];
                roles.toArray(status.roles);
            }
        }
        return status;
    }
}
