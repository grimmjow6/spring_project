package spring.com.core.service.User;

import spring.com.core.dto.PasswordResetDto;
import autonova.com.core.dto.User.*;
import spring.com.core.dto.User.*;
import spring.com.core.model.User.UserAuth;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public interface IUserService extends UserDetailsService {

    UserAuth findByEmail(String email);

    UserAuth save(UserRegistrationDto registration);

    UserAuth update(Long id, UserDto userDto);

    UserAuth updateCompanyDetails(Long id, UserCompanyDetailsDto userCompanyDetailsDto);

    UserAuth updateAddress(Long id, UserAddressDto userAddressDto);

    UserAuth updateDetails(Long id, UserDetailsDto userDetailsDto);

    UserAuth updatePassword(Long id, PasswordResetDto passwordResetDto);

    UserAuth deleteUser(Long id);

    Page<UserAuth> retrieveAllUsers(Pageable pageable, String clientNumber, String email, String phone, String roles);

    void changeUserPassword(UserAuth user, String password);
}
