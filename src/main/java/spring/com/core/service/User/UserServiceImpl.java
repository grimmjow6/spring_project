package spring.com.core.service.User;

import spring.com.core.dto.PasswordResetDto;
import autonova.com.core.dto.User.*;
import spring.com.core.dto.User.*;
import spring.com.core.jpa.specification.user.UserRolesSpecification;
import spring.com.core.jpa.specification.user.UserSpecification;
import spring.com.core.model.Access.Permission;
import spring.com.core.model.Access.Role;
import autonova.com.core.model.User.*;
import spring.com.core.model.User.UserAddress;
import spring.com.core.model.User.UserAuth;
import spring.com.core.model.User.UserCompanyDetails;
import spring.com.core.model.User.UserDetails;
import spring.com.core.repository.Access.PermissionRepository;
import spring.com.core.repository.Access.RoleRepository;
import spring.com.core.repository.User.UserRepository;
import spring.com.core.repository.Users.UsersCarsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private UsersCarsRepository usersCarsRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    private LocalDateTime date = LocalDateTime.now();

    public UserAuth findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    private UserAuth findUserById(Long id){
        return userRepository.findById(id).get();
    }

    public UserAuth save(UserRegistrationDto userRegistrationDto) {
        UserAuth user = new UserAuth();
        UserAddress address = new UserAddress();
        UserDetails details = new UserDetails();
        UserCompanyDetails userCompanyDetails = new UserCompanyDetails();

        user.setEmail(userRegistrationDto.getEmail());
        user.setPassword(passwordEncoder.encode(userRegistrationDto.getPassword()));
        user.setPhone(userRegistrationDto.getPhone());
        user.setCreatedAt(date);

        String typeOfUser = userRegistrationDto.getType();

        if (typeOfUser != null) {
            Role userRoleParticipant = roleRepository.findByName("ROLE_PARTICIPANT");
            Role userRoleServiceProvider = roleRepository.findByName("ROLE_SERVICE_PROVIDER");

            switch (typeOfUser) {
                case "serviceProvider":
                    user.setClientNumber(generateClientNumber("2"));
                    user.setRoles(new HashSet<Role>(Arrays.asList(userRoleServiceProvider)));
                    user.setPhone(userRegistrationDto.getPhone());
                    address.setCity(userRegistrationDto.getCompanyCity());
                    userCompanyDetails.setCompanyName(userRegistrationDto.getCompanyName());
                    userCompanyDetails.setNip(userRegistrationDto.getNip());
                    break;
                case "participant":
                    user.setClientNumber(generateClientNumber("1"));
                    user.setRoles(new HashSet<Role>(Arrays.asList(userRoleParticipant)));
                    break;
                default:
                    break;
            }
        } else {
            Role role = roleRepository.findById(userRegistrationDto.getRoleId()).get();
            String nameOfRole = role.getName();

            switch (nameOfRole) {
                case "ROLE_SERVICE_PROVIDER":
                    user.setClientNumber(generateClientNumber("2"));
                    user.setRoles(new HashSet<Role>(Arrays.asList(role)));
                    user.setPhone(userRegistrationDto.getPhone());
                    address.setCity(userRegistrationDto.getCompanyCity());
                    userCompanyDetails.setCompanyName(userRegistrationDto.getCompanyName());
                    userCompanyDetails.setNip(userRegistrationDto.getNip());
                    break;
                case "ROLE_PARTICIPANT":
                    user.setClientNumber(generateClientNumber("1"));
                    user.setPhone(userRegistrationDto.getPhone());
                    user.setRoles(new HashSet<Role>(Arrays.asList(role)));
                    break;
                default:
                    user.setClientNumber(generateClientNumber("0"));
                    user.setPhone(null);
                    user.setRoles(new HashSet<Role>(Arrays.asList(role)));
                    break;
            }
        }
        user.setCompanyDetails(userCompanyDetails);
        user.setAddress(address);
        user.setDetails(details);

        return userRepository.save(user);
    }

    public UserAuth update(Long id, UserDto userDto) {
        UserAuth user = findUserById(id);

        user.setEmail(user.getEmail());
        user.setPassword(user.getPassword());
        user.setRoles(user.getRoles());
        user.setClientNumber(user.getClientNumber());
        user.getDetails().setName(userDto.getName());
        user.getDetails().setSurname(userDto.getSurname());
        user.setPhone(userDto.getPhone());
        user.setUpdatedAt(date);
        user.getDetails().setUpdatedAt(date);

        return userRepository.save(user);
    }

    public UserAuth updateCompanyDetails(Long id, UserCompanyDetailsDto userCompanyDetailsDto) {
        UserAuth user = findUserById(id);

        user.getCompanyDetails().setCompanyName(userCompanyDetailsDto.getCompanyName());
        user.getCompanyDetails().setNip(userCompanyDetailsDto.getNip());
        user.getCompanyDetails().setRegon(userCompanyDetailsDto.getRegon());
        user.getCompanyDetails().setUpdatedAt(date);

        return userRepository.save(user);
    }

    public UserAuth updateAddress(Long id, UserAddressDto userAddressDto) {
        UserAuth user = findUserById(id);

        user.getAddress().setStreet(userAddressDto.getStreet());
        user.getAddress().setStreetNumber(userAddressDto.getStreetNumber());
        user.getAddress().setFlatNumber(userAddressDto.getFlatNumber());
        user.getAddress().setCity(userAddressDto.getCity());
        user.getAddress().setCountry(userAddressDto.getCountry());
        user.getAddress().setPostcode(userAddressDto.getPostcode());
        user.getAddress().setUpdatedAt(date);

        return userRepository.save(user);
    }

    public UserAuth updateDetails(Long id, UserDetailsDto userDetailsDto) {
        UserAuth user = findUserById(id);

        user.getDetails().setName(userDetailsDto.getName());
        user.getDetails().setSurname(userDetailsDto.getSurname());
        user.getDetails().setUpdatedAt(date);

        return userRepository.save(user);
    }

    public UserAuth updatePassword(Long id, PasswordResetDto passwordResetDto) {
        UserAuth user = findUserById(id);

        user.setPassword(passwordEncoder.encode(passwordResetDto.getNewPassword()));
        user.setUpdatedAt(date);

        return userRepository.save(user);
    }

    @Override
    public void changeUserPassword(final UserAuth user, final String password) {
        user.setPassword(password);
        userRepository.save(user);
    }

    @Override
    public UserAuth deleteUser(Long id) {
        UserAuth user = findUserById(id);

        user.setDeletedAt(date);
        return userRepository.save(user);
    }

    private String generateClientNumber(String number) {
        String lUUID = String.format("%040d", new BigInteger(UUID.randomUUID().toString().replace("-", ""), 16));

        String userClientNumber = lUUID.substring(0, 6).replaceFirst("[0-9]", number);
        UserAuth userClientNumberExist = userRepository.findByClientNumber(userClientNumber);

        while (userClientNumberExist != null) {
            userClientNumber = lUUID.substring(0, 6).replaceFirst("[0-9]", number);
            userClientNumberExist = userRepository.findByClientNumber(userClientNumber);
        }

        return userClientNumber;
    }

    @Override
    @Transactional
    public org.springframework.security.core.userdetails.UserDetails loadUserByUsername(String email)
            throws UsernameNotFoundException {

        UserAuth user = userRepository.findByEmail(email);
        if (user == null) {
            return new org.springframework.security.core.userdetails.User(
                    " ", " ", true, true, true, true,
                    getAuthorities(Arrays.asList(
                            roleRepository.findByName("ROLE_USER"))));
        }

        return new org.springframework.security.core.userdetails.User(
                user.getEmail(), user.getPassword(), getAuthorities(user.getRoles()));
    }
    public Page<UserAuth> retrieveAllUsers(Pageable pageable, String clientNumber, String email, String phone, String rolesStr) {
        Specification<UserAuth> userBaseSpecification = new UserSpecification(email, "email")
                .and(new UserSpecification(phone, "phone"))
                .and(new UserSpecification(clientNumber, "clientNumber"));
        String[] roles = (rolesStr != null) ? rolesStr.split(",") : null;
        Page<UserAuth> page = userRepository.findAll(userBaseSpecification.and(new UserRolesSpecification(roles).build()), pageable);

        return page;
    }
    private Collection<? extends GrantedAuthority> getAuthorities(
            Collection<Role> roles) {

        return getGrantedAuthorities(getPrivileges(roles));
    }

    private List<String> getPrivileges(Collection<Role> roles) {

        List<String> permissions = new ArrayList<>();
        List<Permission> collection = new ArrayList<>();
        for (Role role : roles) {
            collection.addAll(role.getPermissions());
        }
        for (Permission item : collection) {
            permissions.add(item.getName());
        }
        return permissions;
    }

    private List<GrantedAuthority> getGrantedAuthorities(List<String> permissions) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String permission : permissions) {
            authorities.add(new SimpleGrantedAuthority(permission));
        }
        return authorities;
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }



}