package spring.com.core.repository.Users;

import spring.com.core.model.Users.UsersCars;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersCarsRepository extends JpaRepository<UsersCars, Long> {
}
