package spring.com.core.repository.Users;

import spring.com.core.model.Service.Service;
import spring.com.core.model.User.UserAuth;
import spring.com.core.model.Users.UsersServices;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersServicesRepository extends JpaRepository<UsersServices, Long> {

    UsersServices findByUserAuthAndService(UserAuth userId, Service serviceId);

    Page<UsersServices> findByUserAuthId(Long id, Pageable pageable);

    Page<UsersServices> findByUserAuthIdAndService_ServiceCategoryId(Long userId, Long subcatId, Pageable pageable);

    Page<UsersServices> findByUserAuthIdAndService_UserAuthIsNull(Long userId, Pageable pageable);

    Page<UsersServices> findByUserAuthIdAndService_ServiceCategoryIdAndService_UserAuthIsNull(Long userId, Long subcatId, Pageable pageable);

    Page<UsersServices> findByUserAuthIdAndService_UserAuthIsNotNull(Long userId, Pageable pageable);

    Page<UsersServices> findByUserAuthIdAndService_ServiceCategoryIdAndService_UserAuthIsNotNull(Long userId, Long subcatId, Pageable pageable);

    Page<UsersServices> findByUserAuthIdAndActiveTrue(Long userId, Pageable pageable);

    Page<UsersServices> findByUserAuthIdAndActiveFalse(Long userId, Pageable pageable);

    Page<UsersServices> findByUserAuthIdAndActiveTrueAndService_UserAuthIsNull(Long userId, Pageable pageable);

    Page<UsersServices> findByUserAuthIdAndActiveTrueAndService_UserAuthIsNotNull(Long userId, Pageable pageable);

    Page<UsersServices> findByUserAuthIdAndActiveFalseAndService_UserAuthIsNull(Long userId, Pageable pageable);

    Page<UsersServices> findByUserAuthIdAndActiveFalseAndService_UserAuthIsNotNull(Long userId, Pageable pageable);

    Page<UsersServices> findByUserAuthIdAndActiveTrueAndService_ServiceCategoryId(Long userId, Long subcatId, Pageable pageable);

    Page<UsersServices> findByUserAuthIdAndActiveFalseAndService_ServiceCategoryId(Long userId, Long subcatId, Pageable pageable);

    Page<UsersServices> findByUserAuthIdAndActiveTrueAndService_ServiceCategoryIdAndService_UserAuthIsNull(Long userId, Long subcatId, Pageable pageable);

    Page<UsersServices> findByUserAuthIdAndActiveFalseAndService_ServiceCategoryIdAndService_UserAuthIsNull(Long userId, Long subcatId, Pageable pageable);

    Page<UsersServices> findByUserAuthIdAndActiveTrueAndService_ServiceCategoryIdAndService_UserAuthIsNotNull(Long userId, Long subcatId, Pageable pageable);

    Page<UsersServices> findByUserAuthIdAndActiveFalseAndService_ServiceCategoryIdAndService_UserAuthIsNotNull(Long userId, Long subcatId, Pageable pageable);
}
