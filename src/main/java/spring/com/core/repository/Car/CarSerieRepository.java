package spring.com.core.repository.Car;

import spring.com.core.model.Car.CarSerie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarSerieRepository extends JpaRepository<CarSerie, Long> {
}
