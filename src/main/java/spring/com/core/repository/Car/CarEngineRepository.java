package spring.com.core.repository.Car;

import spring.com.core.model.Car.CarEngine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarEngineRepository extends JpaRepository<CarEngine, Long>{
}
