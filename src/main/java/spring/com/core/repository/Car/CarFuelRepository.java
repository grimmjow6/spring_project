package spring.com.core.repository.Car;

import spring.com.core.model.Car.CarFuel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarFuelRepository extends JpaRepository<CarFuel, Long> {
}
