package spring.com.core.repository.Car;

import spring.com.core.model.Car.CarMake;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarMakeRepository extends JpaRepository<CarMake, Long> {
}
