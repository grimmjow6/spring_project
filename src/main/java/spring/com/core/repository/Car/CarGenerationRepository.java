package spring.com.core.repository.Car;

import spring.com.core.model.Car.CarGeneration;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarGenerationRepository extends JpaRepository<CarGeneration, Long> {
}
