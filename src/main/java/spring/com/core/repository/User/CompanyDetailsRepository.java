package spring.com.core.repository.User;

import spring.com.core.model.User.UserCompanyDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyDetailsRepository extends JpaRepository<UserCompanyDetails, Long> {
}
