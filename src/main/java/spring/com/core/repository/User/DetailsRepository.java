package spring.com.core.repository.User;

import spring.com.core.model.User.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DetailsRepository extends JpaRepository<UserDetails, Long> {
}
