package spring.com.core.repository.User;

import spring.com.core.model.Access.Role;
import spring.com.core.model.User.UserAuth;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserAuth, Long>, JpaSpecificationExecutor<UserAuth> {

    UserAuth findByEmail(String email);

    UserAuth findByClientNumber(String clientNumber);

    Page<UserAuth> findByRoles(Role role, Pageable var1);
}

