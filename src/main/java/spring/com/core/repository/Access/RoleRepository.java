package spring.com.core.repository.Access;

import spring.com.core.model.Access.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(String name);

    Role findByNameIgnoreCaseContaining(String name);

}
