package spring.com.core.repository.Access;

import spring.com.core.model.Access.DeviceData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceDataRepository extends JpaRepository<DeviceData, Long> {
//    List<DeviceData> findByUserId(Long userId);

    List<DeviceData> findByUsername(String username);

    DeviceData findByUsernameAndIp(String username, String ip);
}
