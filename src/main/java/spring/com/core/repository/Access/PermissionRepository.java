package spring.com.core.repository.Access;

import spring.com.core.model.Access.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long> {
    Permission findByName(String name);

    Permission findByNameIgnoreCaseContaining(String name);
}
