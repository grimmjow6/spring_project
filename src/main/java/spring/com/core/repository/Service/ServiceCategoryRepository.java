package spring.com.core.repository.Service;

import spring.com.core.model.Service.ServiceCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceCategoryRepository extends JpaRepository<ServiceCategory, Long> {

    ServiceCategory findByNameIgnoreCaseContaining(String name);

    List<ServiceCategory> findByParentIsNull();

    List<ServiceCategory> findByParentIsNotNull();

    List<ServiceCategory> findByParentIsNotNullAndParent_Parent_IdIsNull();

    List<ServiceCategory> findAllByParent_Id(Long id);
}
