package spring.com.core.repository.Service;

import spring.com.core.model.Service.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Long> {

    Service findByIdAndUserAuthIsNull(Long id);

    Service findByIdAndUserAuthIsNotNull(Long id);

    Service findByIdAndUserAuthId(Long serviceId, Long userId);

    Service findByNameIgnoreCaseContaining(String name);

    Service findByDescriptionIgnoreCaseContaining(String desc);

    Page<Service> findByUserAuthIsNull(Pageable pageable);

    Page<Service> findByUserAuthIsNotNull(Pageable pageable);

    Page<Service> findByUserAuthIsNullAndServiceCategory_Id(Long id, Pageable pageable);

    Page<Service> findAllByServiceCategory_Id(Long id, Pageable pageable);

    List<Service> findAllByServiceCategory_Id(Long id);

    Page<Service> findByUserAuthIsNotNullAndServiceCategory_Id(Long id, Pageable pageable);

    List<Service> findAllByServiceCategory_IdAndUserAuthIsNull(Long id);
}
