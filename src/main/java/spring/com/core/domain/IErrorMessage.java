package spring.com.core.domain;

public interface IErrorMessage {
    String DATABASE_ERROR = "Błąd serwera";
    String SUCCESS_TRUE = "Udało się";

    //region user
    String USER_NOT_FOUND = "Nie znaleziono użytkownika z podanym ID";
    String EXIST_EMAIL = "Użytkownik o takim adresie email istnieje już w systemie";
    String EMAIL_NOT_FOUND = "Podany email nie istnieje w systemie";
    String EMAIL_EMPTY = "Nie podano adresu email";

    String PASSWORD_EMPTY = "Nie podano hasła";
    String PASSWORD_CURRENT_EMPTY = "Nie podano obecnego hasła";
    String PASSWORD_NEW_EMPTY = "Nie podano nowego hasła";
    String PASSWORD_REPEAT_EMPTY = "Nie podano powtórzenia hasła";
    String PASSWORD_CURRENT_INCORRECT = "Obecne hasło jest niepoprawne";
    String PASSWORDS_NOT_FIT = "Hasła różnią się od siebie";
    String CREDENTIAL_INCORRECT = "Zły login lub hasło";
    String PASSWORD_NOT_GENERATED = "Hasło nie zostało wygenerowane i email nie został wysłany";

    String EMPTY_NAME = "Nie podano imienia";
    String EMPTY_SURNAME = "Nie podano nazwiska";
    String EMPTY_PHONE = "Nie podano numeru telefonu";
    String EMPTY_COMPANY_NAME = "Nie podano nazwy firmy";
    String EMPTY_NIP = "Nie podano NIP";
    String EMPTY_STREET = "Nie podano ulicy";
    String EMPTY_POSTCODE = "Nie podano kodu pocztowego";
    String EMPTY_COUNTRY = "Nie podano kraju";
    String EMPTY_CITY = "Nie podano miasta";
    //endregion

    //region access
    String EMPTY_ROLE_NAME = "Nie podano nazwy roli";
    String EMPTY_PERMISSION_NAME = "Nie podano nazwy uprawnienia";
    String EXIST_ROLE = "Już istnieje rola o podanej nazwie";
    String PERSMISSION_EXIST = "Już istnieje uprawnienie o podanej nazwie";
    String ROLE_NOT_FOUND = "Nie znaleziono roli o podanym ID";
    String PERMISSION_NOT_FOUND = "Nie znaleziono uprawnienia o podanym ID";
    //endregion

    //region service user
    String EMPTY_SECTION_NAME = "Nie podano nazwy działu";
    String EMPTY_CAT_NAME = "Nie podano nazwy kategorii";
    String EMPTY_SUBCAT_NAME = "Nie podano nazwy podkategorii";
    String EMPTY_SERVICE_NAME = "Nie podano nazwy usługi";
    String EMPTY_SERVICE_DESC = "Nie podano opisu usługi";
    String EMPTY_SERVICE_PRICE = "Nie podano ceny usługi";
    String EXIST_SECTION_NAME = "Już istnieje dział o podanej nazwie";
    String EXIST_CAT_OR_SUBCAT_NAME = "Już istnieje kategoria lub podkategoria o podanej nazwie";
    String EXIST_DF_SERVICE_NAME = "Już istnieje zdefiniowana usługa o podanej nazwie";
    String EXIST_DF_SERVICE_DESC = "Już istnieje zdefiniowana usługa o podanym opisie";
    String EXIST_SERVICE_USER_ACCOUNT = "Już znajduje się na twoim koncie wybrana usługa";
    String EXIST_SERVICE_NAME_USER_ACCOUNT = "Już istnieje usługa stworzona przez Ciebie o podanej nazwie";
    String SECTION_NOT_FOUND = "Nie znaleziono sekcji o podanym ID";
    String CAT_NOT_FOUND = "Nie znaleziono kategorii o podanym ID";
    String SUBCAT_NOT_FOUND = "Nie znaleziono podkategorii o podanym ID";
    String SERVICE_NOT_FOUND = "Nie znaleziono usługi o podanym ID";
    //endregion

}
