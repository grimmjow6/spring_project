package spring.com.core.domain;

public interface IErrorCode {
    int DATABASE_ERROR = 0;
    int SUCCESS_TRUE = 1;

    //region user
    int USER_NOT_FOUND = 2;
    int EXIST_EMAIL = 31;
    int EMAIL_NOT_FOUND = 32;
    int EMAIL_EMPTY = 33;

    int PASSWORD_EMPTY = 41;
    int PASSWORD_CURRENT_EMPTY = 42;
    int PASSWORD_NEW_EMPTY = 43;
    int PASSWORD_REPEAT_EMPTY = 44;
    int PASSWORD_CURRENT_INCORRECT = 45;
    int PASSWORDS_NOT_FIT = 46;
    int CREDENTIAL_INCORRECT = 47;
    int PASSWORD_NOT_GENERATED = 48;

    int EMPTY_NAME = 51;
    int EMPTY_SURNAME = 52;
    int EMPTY_PHONE = 53;
    int EMPTY_COMPANY_NAME = 54;
    int EMPTY_NIP = 55;
    int EMPTY_STREET = 56;
    int EMPTY_POSTCODE = 57;
    int EMPTY_COUNTRY = 58;
    int EMPTY_CITY = 59;
    //endregion

    //region access
    int EMPTY_ROLE_NAME = 21;
    int EMPTY_PERMISSION_NAME = 22;
    int EXIST_ROLE = 23;
    int PERSMISSION_EXIST = 24;
    int ROLE_NOT_FOUND = 25;
    int PERMISSION_NOT_FOUND = 26;
    //endregion

    //region service user
    int EMPTY_SECTION_NAME = 61;
    int EMPTY_CAT_NAME = 62;
    int EMPTY_SUBCAT_NAME = 63;
    int EMPTY_SERVICE_NAME = 64;
    int EMPTY_SERVICE_DESC = 65;
    int EMPTY_SERVICE_PRICE = 66;
    int EXIST_SECTION_NAME = 71;
    int EXIST_CAT_OR_SUBCAT_NAME = 72;
    int EXIST_DF_SERVICE_NAME = 73;
    int EXIST_DF_SERVICE_DESC = 74;
    int EXIST_SERVICE_USER_ACCOUNT = 75;
    int EXIST_SERVICE_NAME_USER_ACCOUNT = 76;
    int SECTION_NOT_FOUND = 77;
    int CAT_NOT_FOUND = 78;
    int SUBCAT_NOT_FOUND = 79;
    int SERVICE_NOT_FOUND = 81;
    //endregion



}
