package spring.com.core.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class Status {

    public int status;
    public String message;
    public String debugMessage;
    public String token;
    public Long id;
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, shape = JsonFormat.Shape.ARRAY)
    public String[] roles;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;

    public Status() {
        timestamp = LocalDateTime.now();
    }

    public Status(int status) {
        this();
        this.status = status;
    }

    public Status(int status, String message) {
        this(status);
        this.message = message;
    }

    public Status(int status, String message, String debugMessage) {
        this(status, message);
        this.debugMessage = debugMessage;
    }

    public Status(int status, String message, Long id, String token) {
        this(status, message);
        this.id = id;
        this.token = token;
    }

    public Status(int status, String message, Long id, String token, String[] roles) {
        this(status, message, id, token);
        this.roles = roles;
    }

}
