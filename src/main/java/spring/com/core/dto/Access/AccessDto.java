package spring.com.core.dto.Access;

import java.util.List;

public class AccessDto {

    private String name;

    private Long[] permissions;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long[] getPermissions() {
        return permissions;
    }

    public void setPermissions(Long[] permissions) {
        this.permissions = permissions;
    }
}
