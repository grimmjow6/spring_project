package spring.com.core.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        String[] str = new String[1];
        str[0] = "*";
        
        registry.addMapping("/**")
                .allowedOrigins(str)
                .allowedMethods("*")
                .allowedHeaders("*");
    }
}
