package spring.com.core.jpa.specification.user;

import spring.com.core.model.User.UserAuth;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import java.util.Stack;

public class UserRolesSpecification {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private Stack<String> roles;
    public UserRolesSpecification(String[] roles) {
        if (roles != null) {
            this.roles = new Stack<>();
            this.roles.addAll(Lists.newArrayList(roles));
        }
    }

    public Specification<UserAuth> build() {
        if (roles == null) {
            return null;
        }
        Specification<UserAuth> spec = new UserRoleSpecification(roles.pop());
        for (String role: this.roles) {
            spec = spec.or(new UserRoleSpecification(role));
        }
        return spec;
    }
}
