package spring.com.core.jpa.specification.user;

import spring.com.core.model.Access.Role;
import spring.com.core.model.User.UserAuth;

import javax.persistence.criteria.*;

public class UserRoleSpecification implements AuthUserSpecification {
    private String role;

    public UserRoleSpecification(String role) {
        this.role = role;
    }
    @Override
    public Predicate toPredicate(Root<UserAuth> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        if (role == null) {
            return criteriaBuilder.isTrue(criteriaBuilder.literal(true));
        }
        Join<UserAuth, Role> roles = root.join("roles");

        return criteriaBuilder.like(roles.get("name"), role);
    }
}
