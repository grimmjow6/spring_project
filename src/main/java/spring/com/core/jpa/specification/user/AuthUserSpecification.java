package spring.com.core.jpa.specification.user;

import spring.com.core.model.User.UserAuth;
import org.springframework.data.jpa.domain.Specification;

public interface AuthUserSpecification extends Specification<UserAuth> {

}
