package spring.com.core.jpa.specification.user;

import spring.com.core.model.User.UserAuth;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public final class UserSpecification implements AuthUserSpecification {
    private String field;
    private String rootField;
    private UserSpecification userSpecification;

    public UserSpecification(String field, String rootField) {
        this.field = field;
        this.rootField = rootField;
    }
    @Override
    public Predicate toPredicate(Root<UserAuth> root, CriteriaQuery<?> q, CriteriaBuilder cb) {
        if (field == null) {
            return cb.isTrue(cb.literal(true));
        }
        return cb.like(root.get(this.rootField), String.format("%%%s%%", this.field));
    }
}
