package spring.com.core.feature.user;

import spring.com.core.domain.Status;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHeaders;
import org.apache.http.entity.ContentType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import utils.TestStringUtils;

import java.io.IOException;
import java.net.URISyntaxException;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApiUsersTests extends TestStringUtils {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    protected TestRestTemplate httpTest;

    @Test
    public void testUnauthorizedResponsesWithoutPassedTokens() throws URISyntaxException {
        String[] paths = usersEndpoints();

        for(String path: paths) {
            ResponseEntity<String> result = httpTest.getForEntity(path, String.class);
            Assert.assertEquals(HttpStatus.UNAUTHORIZED.value(), result.getStatusCodeValue());
        }
    }
    @Test
    public void test404NotFoundJsonContentTypeAndStatus() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String randomUrl = randomStringUrl();
            ResponseEntity<String> result = httpTest.getForEntity(randomUrl, String.class);
            logger.info("Random Url: {}", randomUrl);
            Assert.assertEquals("Random url should return 404 and json content type", result.getStatusCodeValue(), HttpStatus.NOT_FOUND.value());
            Assert.assertEquals("404 status should has content-type json", ContentType.APPLICATION_JSON.toString() ,result.getHeaders().getFirst(HttpHeaders.CONTENT_TYPE));
            Status status = mapper.readValue(result.getBody(), Status.class);

        }
        catch(IOException exception) {
            Assert.assertTrue("IO Problem: " + exception.getMessage(), false);
        }
    }
    @Test
    public void testPostsEndpoints() {

    }
    @Test
    public void testLogin() throws URISyntaxException {
        ObjectMapper mapper = new ObjectMapper();
        String loginUrl = "/v1/api/login";

        // Lookup for get method - should not exist
        ResponseEntity<String> result = httpTest.getForEntity(loginUrl, String.class);
        Assert.assertEquals(result.getStatusCodeValue(), HttpStatus.METHOD_NOT_ALLOWED.value());
        Assert.assertEquals(result.getHeaders().getFirst(HttpHeaders.CONTENT_TYPE), ContentType.APPLICATION_JSON.toString());



    }

    private String[] postUsersEndpoints() {
        return new String[]{"/v1/users", "/v1/users/addresses", "/v1/users/company-details"};
    }
    private String[] usersEndpoints () {
        return new String[]{"/v1/users", "/v1/users/addresses", "/v1/users/company-details"};
    }
}
