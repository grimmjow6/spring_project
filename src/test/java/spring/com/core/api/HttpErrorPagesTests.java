package spring.com.core.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHeaders;
import org.apache.http.entity.ContentType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import utils.TestStringUtils;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)

public class HttpErrorPagesTests extends TestStringUtils {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    protected TestRestTemplate httpTest;

    @Test
    public void show404PageCheckResponseAndContentType() {
        String randomUrl = randomStringUrl();

        // Lookup for get method - should not exist and receive HTTP_NOT_FOUND &&
        ResponseEntity<String> result = httpTest.getForEntity(randomUrl, String.class);
        Assert.assertEquals(
                result.getHeaders().getFirst(HttpHeaders.CONTENT_TYPE).replaceAll("\\s+", ""),
                ContentType.APPLICATION_JSON.toString().replaceAll("\\s+", "")
        );
        Assert.assertEquals(result.getStatusCodeValue(), HttpStatus.NOT_FOUND.value());
        Assert.assertEquals(result.getBody(), HttpStatus.resolve(result.getStatusCodeValue()).getReasonPhrase());
    }
}
