package spring.com.core.service.User;

import spring.com.core.model.Access.Role;
import spring.com.core.model.User.UserAddress;
import spring.com.core.model.User.UserAuth;
import spring.com.core.model.User.UserCompanyDetails;
import spring.com.core.model.User.UserDetails;
import spring.com.core.repository.User.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class UserServiceImplIntegrationTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @TestConfiguration
    static class UserServiceImplIntegrationTestContextConfiguration {

        @Bean
        public IUserService userService() {
            return new UserServiceImpl();
        }

        @Bean
        public BCryptPasswordEncoder bCryptPasswordEncoder(){
            return new BCryptPasswordEncoder();
        }

    }

    @MockBean
    private IUserService userService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @MockBean
    private UserRepository userRepository;

    private LocalDateTime date = LocalDateTime.now();

    @Before
    public void setUp() {
        UserAuth zbyszek = new UserAuth();
        UserAddress address = new UserAddress();
        UserDetails details = new UserDetails();
        UserCompanyDetails userCompanyDetails = new UserCompanyDetails();

        Role role = new Role("ROLE_PARTICIPANT");

        zbyszek.setEmail("zbyszek@test.com");
        zbyszek.setPassword(bCryptPasswordEncoder.encode("1234"));
        zbyszek.setPhone(null);
        zbyszek.setCreatedAt(date);
        zbyszek.setClientNumber("123123");
        zbyszek.setRoles(new HashSet<Role>(Arrays.asList(role)));
        zbyszek.setCompanyDetails(userCompanyDetails);
        zbyszek.setAddress(address);
        zbyszek.setDetails(details);
        zbyszek.setId(10L);

        UserAuth bogdan = new UserAuth();
        bogdan.setEmail("bogdan@test.com");
        bogdan.setPassword(bCryptPasswordEncoder.encode("1234"));
        bogdan.setPhone(null);
        bogdan.setCreatedAt(date);
        bogdan.setClientNumber("123123");
        bogdan.setRoles(new HashSet<Role>(Arrays.asList(role)));
        bogdan.setCompanyDetails(userCompanyDetails);
        bogdan.setAddress(address);
        bogdan.setDetails(details);
        bogdan.setId(20L);

        List<UserAuth> allUsers = Arrays.asList(zbyszek, bogdan);

        Mockito.when(userRepository.findByEmail("wrong_email")).thenReturn(null);
        Mockito.when(userRepository.findByEmail("")).thenReturn(null);
        Mockito.when(userRepository.findByEmail(null)).thenReturn(null);
        Mockito.when(userRepository.findByEmail("testgmail.com")).thenReturn(null);
        Mockito.when(userRepository.findByEmail(zbyszek.getEmail())).thenReturn(zbyszek);
        Mockito.when(userRepository.findByEmail(bogdan.getEmail())).thenReturn(bogdan);
        Mockito.when(userRepository.findById(zbyszek.getId())).thenReturn(Optional.of(zbyszek));
        Mockito.when(userRepository.findAll()).thenReturn(allUsers);
        Mockito.when(userRepository.findById(-99L)).thenReturn(Optional.empty());
    }

    @Test
    public void whenValidEmail_thenUserShouldBeFound() {
        String email = "zbyszek@test.com";
        UserAuth found = userService.findByEmail(email);
        assertThat(found.getEmail()).isEqualTo(email);

    }

    @Test
    public void whenInValidEmail_thenUserShouldNotBeFound() {
        UserAuth fromDb = userRepository.findByEmail("wrong_email");
        assertThat(fromDb).isNull();

        verifyFindByEmailIsCalledOnce("wrong_email");
    }

    @Test
    public void whenEmptyStringEmail_thenUserShouldNotBeFound() {
        UserAuth fromDb = userRepository.findByEmail("");
        assertThat(fromDb).isNull();

        verifyFindByEmailIsCalledOnce("");
    }

    @Test
    public void whenNullEmail_thenUserShouldNotBeFound() {
        UserAuth fromDb = userRepository.findByEmail(null);
        assertThat(fromDb).isNull();

        verifyFindByEmailIsCalledOnce(null);
    }















    @Test
    public void whenValidId_thenUserShouldBeFound() {
        UserAuth fromDb = userRepository.findById(10L).get();
        assertThat(fromDb.getEmail()).isEqualTo("zbyszek@test.com");

        verifyFindByIdIsCalledOnce();
    }

    @Test
    public void whenInValidId_thenEmployeeShouldNotBeFound() {
        Optional<UserAuth> fromDb = userRepository.findById(-99L);
        verifyFindByIdIsCalledOnce();
        assertThat(fromDb).isNull();
    }

    @Test
    public void given2Users_whengetAll_thenReturn2Records() {
        UserAuth zbyszek = new UserAuth();
        UserAddress address = new UserAddress();
        UserDetails details = new UserDetails();
        UserCompanyDetails userCompanyDetails = new UserCompanyDetails();

        Role role = new Role("ROLE_PARTICIPANT");
        zbyszek.setEmail("zbyszek@test.com");
        zbyszek.setPassword(bCryptPasswordEncoder.encode("1234"));
        zbyszek.setPhone(null);
        zbyszek.setCreatedAt(date);
        zbyszek.setClientNumber("123123");
        zbyszek.setRoles(new HashSet<Role>(Arrays.asList(role)));
        zbyszek.setCompanyDetails(userCompanyDetails);
        zbyszek.setAddress(address);
        zbyszek.setDetails(details);
        zbyszek.setId(10L);

        UserAuth bogdan = new UserAuth();

        bogdan.setEmail("bogdan@test.com");
        bogdan.setPassword(bCryptPasswordEncoder.encode("1234"));
        bogdan.setPhone(null);
        bogdan.setCreatedAt(date);
        bogdan.setClientNumber("123123");
        bogdan.setRoles(new HashSet<Role>(Arrays.asList(role)));
        bogdan.setCompanyDetails(userCompanyDetails);
        bogdan.setAddress(address);
        bogdan.setDetails(details);
        bogdan.setId(20L);

        List<UserAuth> allEmployees = userRepository.findAll();
        verifyFindAllEmployeesIsCalledOnce();
        assertThat(allEmployees).hasSize(2).extracting(UserAuth::getEmail).contains(zbyszek.getEmail(), bogdan.getEmail());
    }

    private void verifyFindByEmailIsCalledOnce(String email) {
        Mockito.verify(userRepository, VerificationModeFactory.times(1)).findByEmail(email);
        Mockito.reset(userRepository);
    }

    private void verifyFindByIdIsCalledOnce() {
        Mockito.verify(userRepository, VerificationModeFactory.times(1)).findById(Mockito.anyLong());
        Mockito.reset(userRepository);
    }

    private void verifyFindAllEmployeesIsCalledOnce() {
        Mockito.verify(userRepository, VerificationModeFactory.times(1)).findAll();
        Mockito.reset(userRepository);
    }

}
